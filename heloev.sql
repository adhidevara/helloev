-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Jul 2020 pada 11.59
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `heloev`
--

DELIMITER $$
--
-- Prosedur
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAll` ()  begin
SELECT * FROM registrasiuser;
end$$

--
-- Fungsi
--
CREATE DEFINER=`root`@`localhost` FUNCTION `countevent` () RETURNS INT(11) begin
declare hasil int;
select count(ID_event) into hasil from registrasievent;
return(hasil);
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `ID_Admin` int(11) NOT NULL,
  `Nama` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`ID_Admin`, `Nama`, `Email`, `Password`, `Alamat`) VALUES
(1, 'richard', 'riri@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', 'Boyolali'),
(2, 'ariel', 'ariel@gmail.com', '202cb962ac59075b964b07152d234b70', 'kulon'),
(3, 'Fernaldi', 'Angga@gmail.com', '202cb962ac59075b964b07152d234b70', 'sm'),
(4, 'andi', 'andi@gmail.com', '202cb962ac59075b964b07152d234b70', 'Ngaliyan'),
(5, 'test', 'test@fe.com', '202cb962ac59075b964b07152d234b70', 'test');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_petugas`
--

CREATE TABLE `history_petugas` (
  `ID` int(11) NOT NULL,
  `Nama` varchar(100) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `history_petugas`
--

INSERT INTO `history_petugas` (`ID`, `Nama`, `Email`, `Password`, `Alamat`) VALUES
(5, 'Gusdur', 'Gusdur@gmail.com', '2a8872ca648f969541dd15727492ad0c', 'Kudus'),
(6, 'Burhan', 'Burhan@gmail.com', 'fe9e3203ad8d1387bf8c8d1d889b902b', 'Klaten'),
(0, 'Aan', 'AAN@gmail.com', 'AAN', 'SOELOE'),
(0, 'Nining', 'nining@gmail.com', 'ning', 'Purwokerto'),
(8, 'Gusdur', 'Gusdur@gmail.com', 'merdeka', 'Banjarnegara'),
(3, 'Angga', 'Angga@gmail.com', '1fd5cd9766249f170035b7251e2c6b61', 'jakarta'),
(7, 'Fernaldi', 'Fernaldi06.FA@gmail.com', '123', 'Semarang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE `pembayaran` (
  `ID_pembayaran` int(11) NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `gambar` text NOT NULL,
  `status_pembayaran` varchar(25) NOT NULL,
  `ID_pembeli` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembayaran`
--

INSERT INTO `pembayaran` (`ID_pembayaran`, `total_bayar`, `gambar`, `status_pembayaran`, `ID_pembeli`) VALUES
(1, 125000, '7.jpg', 'Success', 1),
(2, 0, '1.png', 'Success', 2),
(10, 0, '4.png', 'Success', 3),
(11, 0, '5.png', 'Success', 4),
(12, 0, '', 'On Progres', 5),
(13, 0, '', 'On Progres', 6),
(14, 0, '', 'on progres', 7),
(15, 0, '', 'on progres', 9),
(16, 0, '', 'on progres', 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembeli`
--

CREATE TABLE `pembeli` (
  `ID_pembeli` int(11) NOT NULL,
  `ID_user` int(11) NOT NULL,
  `ID_tiket` int(11) NOT NULL,
  `jumlah_tiket` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembeli`
--

INSERT INTO `pembeli` (`ID_pembeli`, `ID_user`, `ID_tiket`, `jumlah_tiket`, `subtotal`) VALUES
(1, 13, 1, 5, 125000),
(2, 20, 1, 1, 25000),
(3, 17, 1, 2, 50000),
(4, 14, 1, 4, 100000),
(5, 13, 1, 2, 50000),
(6, 20, 2, 5, 250000),
(7, 17, 2, 2, 100000),
(8, 14, 2, 1, 50000),
(9, 11, 2, 2, 100000),
(10, 16, 2, 1, 50000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `ID` int(11) NOT NULL,
  `Nama` varchar(20) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Alamat` text NOT NULL,
  `bank` varchar(20) NOT NULL,
  `nama_pemilik` varchar(255) NOT NULL,
  `no_rekening` varchar(255) NOT NULL,
  `kantor_cabang` varchar(50) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  `no_ktp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`ID`, `Nama`, `Email`, `Password`, `Alamat`, `bank`, `nama_pemilik`, `no_rekening`, `kantor_cabang`, `kota`, `foto_ktp`, `no_ktp`) VALUES
(9, 'Gusdur', 'Gusdur@gmail.com', '202cb962ac59075b964b07152d234b70', 'Jakarta', 'Bank BCA', 'gusdur', '098788765556', 'priok', 'Jakarta', 'foto_ktp/gyusdur4.jpg', '0987767566473663'),
(10, 'Fernaldi', 'Fernaldi06.FA@gmail.com', '202cb962ac59075b964b07152d234b70', 'Semarang', '', '', '', '', '', '', ''),
(11, 'Burhan', 'Burhan@gmail.com', 'caf1a3dfb505ffed0d024130f58c5cfa', 'Klaten', 'Bank Mandiri', 'burhan', '12388475754993', 'Semarang', 'semarang', 'foto_ktp/gyusdur1.jpg', '123333215555'),
(12, 'Agung', 'Agung@gmail.com', '202cb962ac59075b964b07152d234b70', 'Boyolali', 'Bank Mandiri', 'Fernaldi Angga', '08888', 'KCP Bandung', 'Bandung', 'foto_ktp/Scan_KTP1.JPG', '0888888'),
(13, 'Richard', 'Rich@gmail.com', '202cb962ac59075b964b07152d234b70', 'Blitar', '', '', '', '', '', '', ''),
(14, 'Anton', 'Anton@gmail.com', '202cb962ac59075b964b07152d234b70', 'Purworejo', 'Bank Mandiri', 'Anton Yosep', '098889876476', 'Purkandri', 'Surakarta', 'foto_ktp/ktp.png', '999367887');

--
-- Trigger `petugas`
--
DELIMITER $$
CREATE TRIGGER `TRG_history` BEFORE DELETE ON `petugas` FOR EACH ROW begin
insert into history_petugas
set
ID= old.ID,
Nama= old.Nama,
Email = old.Email,
Password = old.Password,
Alamat = old.Alamat;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `registrasievent`
--

CREATE TABLE `registrasievent` (
  `ID_event` int(11) NOT NULL,
  `ID` int(11) NOT NULL,
  `penyelenggara` varchar(255) NOT NULL,
  `nama_event` varchar(255) NOT NULL,
  `tgl_acara` date NOT NULL,
  `waktu_mulai_acara` varchar(255) NOT NULL,
  `waktu_berakhir_acara` varchar(255) NOT NULL,
  `lokasi` text NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` text NOT NULL,
  `Status` varchar(20) NOT NULL,
  `alasan` text NOT NULL,
  `harga_tiket` int(11) NOT NULL,
  `jumlah_stok_tiket` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `registrasievent`
--

INSERT INTO `registrasievent` (`ID_event`, `ID`, `penyelenggara`, `nama_event`, `tgl_acara`, `waktu_mulai_acara`, `waktu_berakhir_acara`, `lokasi`, `deskripsi`, `gambar`, `Status`, `alasan`, `harga_tiket`, `jumlah_stok_tiket`) VALUES
(15, 10, 'Telkom University', 'Block Chain', '2019-05-04', '10.00 AM', '12.00 PM', 'JL. Buah Batu Blok C77', 'Revolusi Industri 4.0', 'blok.jpg', 'Tervalidasi', 'Panitia ne ngaco', 25000, 10000),
(16, 10, 'Ismaya', 'LaLaLa Festival', '2019-05-03', '10.00 AM', '01.00 PM', 'JL. Cikole raya', 'Festival musik ditengah hutan pertama diindonesia', 'lalala.jpg', 'Tervalidasi', '', 50000, 2000),
(17, 14, 'Indofood', 'Teh pucuk food festival', '2019-05-11', '09.00 AM', '09.00 PM', 'Lapangan Pussenif RE Martadhinata', 'Festival makanan', 'teh.jpg', 'Tervalidasi', '', 10000, 1500),
(18, 9, 'Athronika', 'FROM 2 WITH LOVE', '2019-05-05', '10.00 PM', '23.59 AM', 'Lapangan Secapa AD', 'Setiap tahunnya SMAN 2 Bandung mengadakan event besar yaitu From 2 With Love atau lebih dikenal dengan F2WL. Tahun ini, F2WL menamakan bazar mereka dengan nama “ATHRONIKA” yang bertemakan tentang teknologi yang berkembang pada zaman ini.\r\n\r\nAcara ini dimeriahkan oleh banyak artis tanah air, yaitu Jason Ranti, GBS, Marcell, GAC, The Changcuters, Marion Jola, Yura Yunita, The Overtunes, RAN, dan KAHITNA. Danang dan Darto juga ikut memeriahkan acara sebagai pembawa acara atau MC di F2WL kali ini.\r\n\r\nTahun 2019 ini, F2WL diadakan di Lapangan Pussenif yang ada di Jl. Brigadir Jend. Katamso No. 31, Cihaur Geulis pada 9 Februari 2019.\r\n\r\nBanyak masyarakat yang antusias pada bazar ini, buktinya sejak awal open gate pada pukul 10.00 sudah ada pengunjung yang mengantri di ticket box. Semakin siang, semakin banyak pengunjung yang berdatangan. Pengunjung yang datang dari berbagai kalangan mulai dari pelajar hingga orang tua.\r\n\r\nTata panggung di bazar kali ini juga sangat menakjubkan, kenapa? Karena diatas panggung tedapat logo “ATHRONIKA” yang berganti-ganti warna tiap menitnya. Tata lampunya juga patut kita apresiasi karena sangat menakjubkan dan teratur. Selain itu, di venue juga terdapat banyak spot foto yang menarik diantaranya ada logo F2WL yang berupa sayap putih yang ada di depan pintu masuk dan ada juga tulisan F2WL’19 yang menyala saat malam hari. Saat kita akan masuk ke venue, di jalan masuk dihiasi oleh besi-besi berbentuk persegi yang menyala pada malam hari dan menambah kesan instagramable jika berfoto disitu.', 'f2wl.jpg', 'Tervalidasi', '', 60000, 5000),
(19, 13, 'Telkom University', 'Seminar Interpreuner', '2019-05-06', '09.30 AM', '12.00 PM', 'Gedung Serba Guna Telkom University', 'Pengenalan tentang industri kreative di telkom yang berkembang dijaman millenial', 'seminar.jpg', 'Tervalidasi', '', 15000, 500),
(20, 12, 'Telkom', 'Seminar Bitcoin', '2019-05-09', '10.00 AM', '12.00 PM', 'BUAH BATU', 'PEngenalan tentang interpreuner', '', 'Belum Valid', '', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `registrasiuser`
--

CREATE TABLE `registrasiuser` (
  `ID_user` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `noktp` varchar(25) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nohp` varchar(15) NOT NULL,
  `Status_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `registrasiuser`
--

INSERT INTO `registrasiuser` (`ID_user`, `username`, `password`, `noktp`, `email`, `nohp`, `Status_user`) VALUES
(11, 'Kamalamaw', '202cb962ac59075b964b07152d234b70', '0333223882983746', 'antonandre@student.telkomuniversity.ac.id', '0821178964543', 'Berhasil'),
(13, 'Burhan', '202cb962ac59075b964b07152d234b70', '0992839837423480', 'antonandre7@gmail.com', '082278198279', 'Berhasil'),
(14, 'Angga', '202cb962ac59075b964b07152d234b70', '0033329857746830', 'Fernaldi@gmail.com', '08219948576', 'Belum Berhasil'),
(15, 'Firza', '73aeaa1bed34eb82aa04df2d2a6adf91', '0333223882983442', 'firzamaulana@gmail.com', '08581133187', 'Belum Berhasil'),
(16, 'Rayan', '8657359bc53e0b7237858af08bcd3e8a', '0223223882983746', 'Rayan@gmail.com', '08133197781', 'Belum Berhasil'),
(17, 'Alvin', 'f8ac8ebc53fd3b020ad526a5dfb1cf3f', '09987876545454', 'Alvin@gmail.com', '09898767644', 'Belum Berhasil'),
(18, 'Ikhsan', 'b2ab8163dc9a4ffdb8dcea1b0a4230f4', '0989876564874836', 'ikhsan@gmail.com', '08123454321', 'Belum Berhasil'),
(19, 'Rifqi', '6c8dab289527fc0927aa7e6507898bdd', '098984141874836', 'rifqi@gmail.com', '08123232321', 'Belum Berhasil'),
(20, 'Abror', 'd376f2bd6175a95e11fa0901fe5c3952', '0333567890983746', 'Abror@gmail.com', '08576858498', 'Belum Berhasil'),
(22, 'Faizal', '3adbf84fcc3478d48d608190a72187f3', '0999223882983746', 'Faizal@gmail.com', '08211764565', 'Belum Berhasil'),
(23, 'Rafata', '738e699be1845424b87e841c239a7b4d', '0990011182983746', 'Rafata@gmail.com', '08211788565', 'Belum Berhasil');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tiket`
--

CREATE TABLE `tiket` (
  `ID_tiket` int(11) NOT NULL,
  `ID_event` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `stok_tiket` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tiket`
--

INSERT INTO `tiket` (`ID_tiket`, `ID_event`, `harga`, `stok_tiket`) VALUES
(1, 15, 25000, 10000),
(2, 16, 50000, 2000),
(3, 17, 10000, 1500),
(4, 18, 60000, 5000),
(5, 19, 15000, 500);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `ID_pembayaran` int(11) NOT NULL,
  `ID_pembeli` int(11) NOT NULL,
  `ID_user` int(11) NOT NULL,
  `ID_event` int(11) NOT NULL,
  `ID_tiket` int(11) NOT NULL,
  `tanggal_dibeli` datetime NOT NULL DEFAULT current_timestamp(),
  `nama_event` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `jumlah_tiket` varchar(255) NOT NULL,
  `harga` varchar(255) NOT NULL,
  `subtotal` varchar(255) NOT NULL,
  `tgl_acara` date NOT NULL,
  `lokasi` text NOT NULL,
  `status_pembayaran` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `ID_pembayaran`, `ID_pembeli`, `ID_user`, `ID_event`, `ID_tiket`, `tanggal_dibeli`, `nama_event`, `username`, `jumlah_tiket`, `harga`, `subtotal`, `tgl_acara`, `lokasi`, `status_pembayaran`) VALUES
(8, 1, 1, 13, 15, 1, '2020-07-16 14:33:01', 'Block chain', 'Burhan', '5', '25000', '125000', '2019-05-04', 'JL. Buah Batu Blok C77\r\n', 'Success'),
(9, 2, 9, 11, 15, 1, '2020-07-16 14:33:01', 'Block Chain', 'Kamalamaw', '4', '25000', '100000', '2019-05-04', 'JL. Buah Batu Blok C77', 'Success'),
(10, 12, 3, 17, 16, 2, '2020-07-17 16:52:35', 'LaLaLa Festival', 'Alvin', '9', '50000', '450000', '2019-05-03', 'Jl. Cikole raya', 'Success'),
(11, 10, 4, 14, 17, 3, '2020-07-17 16:52:35', 'Teh pucuk food festival', 'Angga', '8', '10000', '80000', '2019-05-11', 'Lapangan Pussenif RE Martadhinata', 'Succed'),
(12, 11, 5, 15, 18, 4, '2020-07-17 16:58:22', 'From two with love', 'Firza', '9', '60000', '540000', '2019-05-05', 'Lapangan Secapa AD', 'Success'),
(13, 13, 6, 16, 19, 5, '2020-07-17 16:58:22', 'Seminar Interprenuer', 'Rayan', '8', '15000', '120000', '2019-05-06', 'Gedung Serba Guna Telkom University', 'Success'),
(14, 14, 7, 18, 20, 5, '2020-07-17 16:58:22', 'Seminar Bitcoin', 'Ikhsan', '9', '15000', '135000', '2019-05-09', 'BUAH BATU', 'Success');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ID_Admin`);

--
-- Indeks untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`ID_pembayaran`),
  ADD KEY `ID_pembeli` (`ID_pembeli`);

--
-- Indeks untuk tabel `pembeli`
--
ALTER TABLE `pembeli`
  ADD PRIMARY KEY (`ID_pembeli`),
  ADD KEY `ID_user` (`ID_user`),
  ADD KEY `ID_tiket` (`ID_tiket`);

--
-- Indeks untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`ID`);

--
-- Indeks untuk tabel `registrasievent`
--
ALTER TABLE `registrasievent`
  ADD PRIMARY KEY (`ID_event`);

--
-- Indeks untuk tabel `registrasiuser`
--
ALTER TABLE `registrasiuser`
  ADD PRIMARY KEY (`ID_user`);

--
-- Indeks untuk tabel `tiket`
--
ALTER TABLE `tiket`
  ADD PRIMARY KEY (`ID_tiket`),
  ADD KEY `ID_event` (`ID_event`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `ID_pembayaran` (`ID_pembayaran`),
  ADD KEY `ID_pembeli` (`ID_pembeli`),
  ADD KEY `ID_user` (`ID_user`),
  ADD KEY `ID_event` (`ID_event`),
  ADD KEY `ID_tiket` (`ID_tiket`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `ID_Admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `ID_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `pembeli`
--
ALTER TABLE `pembeli`
  MODIFY `ID_pembeli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `petugas`
--
ALTER TABLE `petugas`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `registrasievent`
--
ALTER TABLE `registrasievent`
  MODIFY `ID_event` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `registrasiuser`
--
ALTER TABLE `registrasiuser`
  MODIFY `ID_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `tiket`
--
ALTER TABLE `tiket`
  MODIFY `ID_tiket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `pembayaran_ibfk_1` FOREIGN KEY (`ID_pembeli`) REFERENCES `pembeli` (`ID_pembeli`);

--
-- Ketidakleluasaan untuk tabel `pembeli`
--
ALTER TABLE `pembeli`
  ADD CONSTRAINT `pembeli_ibfk_2` FOREIGN KEY (`ID_user`) REFERENCES `registrasiuser` (`ID_user`),
  ADD CONSTRAINT `pembeli_ibfk_3` FOREIGN KEY (`ID_tiket`) REFERENCES `tiket` (`ID_tiket`);

--
-- Ketidakleluasaan untuk tabel `tiket`
--
ALTER TABLE `tiket`
  ADD CONSTRAINT `tiket_ibfk_1` FOREIGN KEY (`ID_event`) REFERENCES `registrasievent` (`ID_event`);

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`ID_pembayaran`) REFERENCES `pembayaran` (`ID_pembayaran`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`ID_user`) REFERENCES `registrasiuser` (`ID_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`ID_pembeli`) REFERENCES `pembeli` (`ID_pembeli`),
  ADD CONSTRAINT `transaksi_ibfk_4` FOREIGN KEY (`ID_event`) REFERENCES `registrasievent` (`ID_event`),
  ADD CONSTRAINT `transaksi_ibfk_5` FOREIGN KEY (`ID_tiket`) REFERENCES `tiket` (`ID_tiket`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
