<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class m_model extends CI_Model 
{
	public function tampildata( $halaman,$jumlah)
	{
		return $this->db->query("SELECT * FROM petugas LIMIT $halaman,$jumlah");
	}

	public function tampildataevent( $halaman,$jumlah)
	{
		return $this->db->query("SELECT *, (tiket.stok_tiket - SUM(jumlah_tiket)) AS 'sisa', SUM(jumlah_tiket) AS terjual FROM transaksi
		RIGHT JOIN registrasievent
			ON transaksi.ID_event = registrasievent.ID_event
		LEFT JOIN tiket
			ON transaksi.ID_tiket = tiket.ID_tiket
		GROUP BY registrasievent.ID_event
		 LIMIT $halaman,$jumlah");
	}

	public function tampildatauser( $halaman,$jumlah)
	{
		//procedure
		return $this->db->query('call getAll()');
		// return $this->db->query("SELECT * FROM registrasiuser LIMIT $halaman,$jumlah");
	}
	public function tampildatapembeli( $halaman,$jumlah)
	{
		return $this->db->query("SELECT ID_event,penyelenggara, nama_event,tgl_acara,Status from registrasievent WHERE Status='Tervalidasi'");
	}
	public function tampildatatiket( $halaman,$jumlah)
	{
		// $this->db->join('tiket', 'pembeli.ID_tiket = tiket.ID_tiket');
		// 	$this->db->join('registrasievent', 'tiket.ID_event = registrasievent.ID_event');
		// 			$this->db->join('pembayaran', 'pembeli.ID_pembeli = pembeli.ID_pembeli');
		// $this->db->join('registrasiuser', 'registrasiuser.ID_user = pembeli.ID_user');
	

		
		
		
		// return $this->db->get('pembeli',$halaman,$jumlah);
		
		 return $this->db->query("SELECT * FROM pembeli 
		 JOIN tiket USING(ID_tiket) 
		 JOIN registrasievent USING(ID_event) 
		 JOIN pembayaran USING(ID_pembeli) 
		 JOIN registrasiuser USING(ID_user)");
	}

    function input_data($data,$table){
		$this->db->insert($table,$data);
    }
    
    function cek_login($Email,$Password){		
		// return $this->db->get_where($table,$where);
			$this->db->select("*");
			$this->db->from("petugas");
			$this->db->where("Email",$Email);
			$this->db->where("Password",$Password);
			return $this->db->get()->result_array();
	}

	function cek_login_admin($Email,$Password){		
		// return $this->db->get_where($table,$where);
			$this->db->select("*");
			$this->db->from("admin");
			$this->db->where("Email",$Email);
			$this->db->where("Password",$Password);
			return $this->db->get()->result_array();
	}

	public function getnama($email)
	{
		return $this->db->query("SELECT Nama FROM petugas WHERE Email='$email'");
	}

    function tampil_data(){
		return $this->db->get('petugas');
    }
    
    function tampil_data_event(){
		return $this->db->get('registrasievent');
    }

    function tampil_data_user(){
		return $this->db->get('registrasiuser');
		}
		function tampil_data_pembeli(){
			return $this->db->query("SELECT * FROM pembeli JOIN tiket USING(ID_tiket) JOIN registrasievent USING(ID_event) JOIN pembayaran USING(ID_pembeli) JOIN registrasiuser USING(ID_user)");
			}
		function tampil_data_tiket(){
			return $this->db->query("SELECT * FROM registrasievent JOIN tiket USING(ID_event) JOIN pembeli USING(ID_tiket) JOIN registrasiuser USING(ID_user)");
		}



    function hapus_data_karyawan($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
		}
		
		public function hapus_data($ID_event)
    {
        $this->db->where('ID_event', $ID_event);
       $this->db->delete('registrasievent', ['ID_event' => $ID_event ]); 

		}
		
		public function hapus_data_user($ID_user)
    {
        $this->db->where('ID_user', $ID_user);
       $this->db->delete('registrasiuser', ['ID_user' => $ID_user ]); 

		}
		
		function edit_petugas($where,$table){	
			// $this->db->join('tiket', 'tiket.ID_event = registrasievent.ID_event');
			return $this->db->get_where($table,$where);
			}
    
    function edit_data($where,$table){	
			$this->db->join('tiket', 'tiket.ID_event = registrasievent.ID_event');
		return  $this->db->get_where($table,$where);	
		}

		function edit_data_user($where,$table){	
			// $this->db->join('registrasievent', 'registrasievent.ID_tiket = tiket.ID_tiket');
		return  $this->db->get_where($table,$where);	
		}
		
		function edit_data_pembeli($where,$table){		
		return $this->db->query("SELECT * FROM pembeli JOIN tiket USING(ID_tiket) JOIN registrasievent USING(ID_event) JOIN pembayaran USING(ID_pembeli) JOIN registrasiuser USING(ID_user)
		where ID_pembeli = '$where' ");

			}
			
    
    function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
		}

		function updatetiketby($id,$data)
		{
			# code...
			$this->db->where('ID_event', $id);
			$this->db->update('tiket', $data);
		}

		function updateeventby($id,$data)
		{
			# code...
			$this->db->where('ID_event', $id);
			$this->db->update('registrasievent', $data);
		}
		
		function update_data_pembeli($id,$data){
		$this->db->where('ID_pembeli', $id);
		$this->db->update('pembayaran', $data);
		
		
		
		
		

			}

    public function cariorang()
	{
		$cari = $this->input->POST('cari', TRUE);
		$data = $this->db->query("SELECT * from petugas where Nama like '%$cari%' ");
		return $data->result();
  }
  public function carievent()
	{
		$cari = $this->input->POST('cari', TRUE);
		$data = $this->db->query("SELECT * from registrasievent JOIN tiket USING(ID_event) where penyelenggara like '%$cari%' ");
		return $data->result();
  }
  public function cariuser()
	{
		$cari = $this->input->POST('cari', TRUE);
		$data = $this->db->query("SELECT * from registrasiuser where username like '%$cari%' ");
		return $data->result();
	}
	public function caripembeli()
	{
		$cari = $this->input->POST('cari', TRUE);
		$data = $this->db->query("SELECT * FROM pembeli JOIN tiket USING(ID_tiket) JOIN registrasievent USING(ID_event) JOIN pembayaran USING(ID_pembeli) JOIN registrasiuser USING(ID_user) where  penyelenggara like '%$cari%' group by penyelenggara");
		return $data->result();
	}
	public function caritiket()
	{
		$cari = $this->input->POST('cari', TRUE);
		$data = $this->db->query("SELECT * FROM pembeli JOIN tiket USING(ID_tiket) JOIN registrasievent USING(ID_event) JOIN pembayaran USING(ID_pembeli) JOIN registrasiuser USING(ID_user) where username like '%$cari%' group by username ");
		return $data->result();
	}
// ====================================================================================================
	public function CountEvent()
	{
		//function
		$data = $this->db->select("countevent() as jumlah_event");
		return $data->get();

	}
	
	public function CountEventValid()
	{
		$this->db->select("count(ID_event)  as jumlah_event_valid");
		$this->db->where('Status', 'Tervalidasi');
		$this->db->group_by('Status');
		$data = $this->db->from("registrasievent")->get();
		return $data->row();
	}

	public function CountEventBelumValid()
	{
		$this->db->select("count(ID_event)  as jumlah_event_belum_valid");
		$this->db->where('Status', 'Belum Valid');
		$this->db->group_by('Status');
		$data = $this->db->from("registrasievent")->get();
		return $data->row();
	}


// ====================================================================================================
	
	public function CountUser()
	{
		$this->db->select("count(ID_user) as jumlah_user");
		$data = $this->db->from("registrasiuser")->get();
		return $data->row();

	}

	public function CountUserBerhasil()
	{
		$this->db->select("count(ID_user) as jumlah_user_berhasil");
		$this->db->where('Status_user', 'Berhasil');
		$this->db->group_by('Status_user');
		$data = $this->db->from("registrasiuser")->get();
		return $data->row();

	}

	public function CountUserBelumBerhasil()
	{
		$this->db->select("count(ID_user) as jumlah_user_belum_berhasil");
		$this->db->where('Status_user', 'Belum Berhasil');
		$this->db->group_by('Status_user');
		$data = $this->db->from("registrasiuser")->get();
		return $data->row();

	}

	// ====================================================================================================
	public function ConfirmStatus($id_event)
	{
		$data = array( 'Status' => "Tervalidasi");
		// $data['Status'] = "Belum Valid";
		$this->db->where('ID_event', $id_event);
		
		$this->db->update('registrasievent', $data);

		
	}



	public function ConfirmStatusUser($id_user)
	{
		$data = array( 'Status_user' => "Berhasil");
		// $data['Status'] = "Belum Valid";
		$this->db->where('ID_user', $id_user);
		
		$this->db->update('registrasiuser', $data);

		
	}

	public function get_event()
    {
				
				// $data = $this->db->join('tiket', 'tiket.ID_event = registrasievent.ID_event');
				// $data = $this->db->get('registrasievent');
				$data = $this->db->query("SELECT * FROM pembeli JOIN tiket USING(ID_tiket) JOIN registrasievent USING(ID_event) JOIN pembayaran USING(ID_pembeli) JOIN registrasiuser USING(ID_user)");
        return $data->result_array();
		}
		
		public function get_event1($id)
    {
		$data = $this->db->query("SELECT * FROM transaksi where ID_event = '$id' ORDER BY tanggal_dibeli ASC ");
       return $data;
    }
		public function get_tiket()
    {
			$data = $this->db->query("SELECT * FROM registrasievent JOIN tiket USING(ID_event) JOIN pembeli USING(ID_tiket) JOIN registrasiuser USING(ID_user)"
			);
      return $data->result_array();
		}
		public function get_tiket1($id)
    {
			$data = $this->db->query("SELECT * FROM transaksi where ID_user='$id' and status_pembayaran='Success' ORDER BY tanggal_dibeli ASC");
      return $data->result_array();
	}
	
// =============generate ID==============================================================================================================================================================================
	public function FunctionName($table,$primaryKey,$kodedepan)
	{
		$this->db->select('Right('.$table.'.'.$primaryKey.',4) as kode', FALSE);
		$this->db->order_by('$primaryKey', 'desc');
		$this->db->limit(1);
		$query = $this->db->get($table);
		if ($query->num_rows() <> 0) {
			$data = $query->row();
			$kode = intval($data->kode) + 1;
		}
		else {
			$kode = 1;
		}
		$kodemax = str_pad($kode,4,"0",STR_PAD_LEFT);
		$kodejadi = $kodedepan.$kodemax;
		return $kodejadi;
		
		
	}
	
	public function CountCancelEvent()
	{
		$this->db->from('registrasievent');
		$this->db->where('Status', 'Pengajuan Pembatalan');
		$this->db->or_where('Status', 'Canceled By Admin');
		return $this->db->get();
	}

	public function CountApprovalCancelEvent()
	{
		$this->db->from('registrasievent');
		$this->db->where('Status', 'Pengajuan Pembatalan');
		return $this->db->get();
	}

	public function listCancelEvent()
	{
		$this->db->from('registrasievent');
		$this->db->where('Status', 'Pengajuan Pembatalan');
		$this->db->or_where('Status', 'Canceled By Admin');
		return $this->db->get();
	}

	public function updateStatus($id, $data)
	{
		$this->db->where('ID_event', $id);
		$this->db->update('registrasievent', $data);
	}

	public function getBuyers($id)
	{
		return $this->db->query("
		SELECT * FROM transaksi
		JOIN registrasiuser ON transaksi.ID_user = registrasiuser.ID_user 
		JOIN registrasievent ON transaksi.ID_event = registrasievent.ID_event
		WHERE transaksi.ID_event = ".$id);
	}


	public function sisakuota($halaman,$jumlah)
	{
		return $this->db->query(
			"SELECT *, (tiket.stok_tiket - SUM(jumlah_tiket)) AS 'sisa', SUM(jumlah_tiket) AS terjual 
			FROM transaksi 
			JOIN tiket ON transaksi.ID_tiket = tiket.ID_tiket
			JOIN registrasievent ON transaksi.ID_event = registrasievent.ID_event JOIN petugas ON petugas.ID = registrasievent.ID
			GROUP BY transaksi.ID_event
			LIMIT ".$halaman.", ".$jumlah);
	}
}

/* End of file ModelName.php */

?>
