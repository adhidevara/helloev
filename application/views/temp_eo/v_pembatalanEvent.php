<!DOCTYPE html>
<html>
<head>
	
</head>
<body>
	
	<?php foreach($user as $u){ ?>
	<form action="<?php echo base_url(). 'controller_eo/proPembatalanEvent'; ?>" method="post" enctype="multipart/form-data">
		<table style="margin:20px auto;">
			<tr>
				<td>Penyelenggara</td>
				<td>
					<input type="hidden" name="ID_event" value="<?php echo $u->ID_event ?>">
					<input type="text" class="form-control form-control-user" name="penyelenggara" required="required" value="<?php echo $u->penyelenggara ?>" disabled>
				</td>
			</tr>
			<tr>
				<td>Nama Event</td>
				<td><input type="text" class="form-control form-control-user" name="nama_event" required="required" value="<?php echo $u->nama_event ?>" disabled></td>
			</tr>
			<tr>
				<td>Tanggal Acara</td>
				<td><input type="text" class="form-control form-control-user"  name="tgl_acara" required="required" value="<?php echo $u->tgl_acara ?>" disabled></td>
            </tr>
            <tr>
				<td>Waktu Mulai Acara</td>
				<td><input type="text" class="form-control form-control-user" name="waktu_mulai_acara" required="required" value="<?php echo $u->waktu_mulai_acara ?>" disabled></td>
			</tr>
            <tr>
				<td>Waktu Berakhir Acara</td>
				<td><input type="text" class="form-control form-control-user" name="waktu_berakhir_acara" required="required" value="<?php echo $u->waktu_berakhir_acara ?>" disabled></td>
			</tr>
            <tr>
				<td>Lokasi</td>
				<td><input type="text" class="form-control form-control-user" name="lokasi" required="required"  value="<?php echo $u->lokasi ?>" disabled></td>
			</tr>
            <tr>
				<td>Alasan Pembatalan Event</td>
				<td><textarea class="form-control form-control-user" name="alasan" required="required"></textarea></td>
			</tr>
			<tr>
				<td>Berkas Pembatalan Event</td>
				<td><input type="file" class="form-control form-control-user" name="berkas_pembatalan" required="required"></textarea></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" class="btn btn-primary btn-user btn-block"	 name="submit" value="Simpan"></td>
			</tr>
		</table>
	</form>	
	<?php } ?>
</body>
</html>