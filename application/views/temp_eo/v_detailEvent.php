<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin  - Dashboard</title>

  <!-- Custom fonts for this template -->
  <link href="<?php echo base_url() ?>file/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo base_url() ?>file/css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="<?php echo base_url() ?>file/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  
  <link rel="shortcut icon" href="<?php echo base_url() ?>file/img/icon2.png">

  <script>
    function printContent(el) {
        var resPage = document.body.innerHTML;
        var printCont = document.getElementById(el).innerHTML;
        document.body.innerHTML = printCont;
        window.print();
        document.body.innerHTML = resPage;
    }
  </script>

</head>


        <!-- Begin Page Content -->
        <div id="div-print" class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Detail Event</h1>
          
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">  
            <h6 class="m-0 font-weight-bold text-primary">&nbsp&nbsp&nbsp&nbspMasukkan Tanggal Data yang ingin dicari</h6>
            <form method="post" action="<?php echo base_url() ?>controller_eo/detail_event/<?=$data[0]->ID_event?>" class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            Dari <input type="date" name="dari" class="form-control bg-light">
            Sampai <input type="date" name="sampai" class="form-control bg-light">
            <button type="submit" name="submit" class="btn btn-primary">Cari</button><br>
            </form>
            <button onclick="printContent('div-print')" class="btn btn-danger">Print</button>
              <div class="card-body">
              <div class="table-responsive">
              
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <tr>
                <th>Nama Event</th>
            </tr>
            <tr>
                <td><?=$data[0]->nama_event?></td>
            </tr>
        </table>
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <tr>
            <th>No</th>
            <th>Tanggal Dibeli</th>
            <th>Nama Pembeli </th>
            <th>Harga</th>
            <th>Jumlah</th>
            <th>Subtotal</th>
            

		</tr>
		<?php
    $no = 1;
		foreach($data as $u){ 
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $u->tanggal_dibeli ?></td>
			<td><?php echo $u->username ?></td>
			<td><?php echo $u->harga ?></td>
			<td><?php echo $u->jumlah_tiket ?></td>
            <td><?php echo $u->subtotal ?></td>
		</tr>
		<?php } ?>
	</table>
    <i>
    <?php
        if(isset($_POST['submit'])){
            echo $info;
        }
        else{
            $info = "*Data Keseluruhan Pembelian Tiket";
            echo $info;
        }
    ?>
    </i>
            </div>
            </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">


      
