<!DOCTYPE html>
<html>
<head>
	
</head>
<body>
	
	<?php foreach($user as $u){ ?>
	<form action="<?php echo base_url(). 'controller_eo/updateevent_eo'; ?>" method="post" enctype="multipart/form-data">
		<table style="margin:20px auto;">
			<tr>
				<td>Penyelenggara</td>
				<td>
					<input type="hidden" name="ID_event" value="<?php echo $u->ID_event ?>">
					<input type="text" class="form-control form-control-user" name="penyelenggara" required="required" value="<?php echo $u->penyelenggara ?>">
				</td>
			</tr>
			<tr>
				<td>Nama Event</td>
				<td><input type="text" class="form-control form-control-user" name="nama_event" required="required" value="<?php echo $u->nama_event ?>"></td>
			</tr>
			<tr>
				<td>Tanggal Acara</td>
				<td><input type="text" class="form-control form-control-user"  name="tgl_acara" required="required" value="<?php echo $u->tgl_acara ?>"></td>
            </tr>
            <tr>
				<td>Waktu Mulai Acara</td>
				<td><input type="text" class="form-control form-control-user" name="waktu_mulai_acara" required="required" value="<?php echo $u->waktu_mulai_acara ?>"></td>
			</tr>
            <tr>
				<td>Waktu Berakhir Acara</td>
				<td><input type="text" class="form-control form-control-user" name="waktu_berakhir_acara" required="required" value="<?php echo $u->waktu_berakhir_acara ?>"></td>
			</tr>
            <tr>
				<td>Lokasi</td>
				<td><input type="text" class="form-control form-control-user" name="lokasi" required="required"  value="<?php echo $u->lokasi ?>"></td>
			</tr>
            <tr>
				<td>Harga</td>
				<td><input type="text" class="form-control form-control-user" name="harga" required="required" value="<?php echo $u->harga ?>"></td>
			</tr>
            <tr>
				<td>Jumlah Ticket</td>
				<td><input type="text" class="form-control form-control-user" name="stok_tiket" required="required" value="<?php echo $u->stok_tiket ?>"></td>
			</tr>
			<tr>
				<td>Upload Berkas Event</td>
				<td><input type="file" class="form-control form-control-user" name="berkas_event" required="required" value=""></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" class="btn btn-primary btn-user btn-block"	 name="submit" value="Simpan"></td>
			</tr>
		</table>
	</form>	
	<?php } ?>
</body>
</html>