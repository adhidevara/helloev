<!-- Striped Rows -->
            <div class="row clearfix">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" style="padding-left: 100px">
                    <div class="card">
                        <div class="body table-responsive">
                          <?php foreach ($informasi as $i) {?>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Informasi Rekening Saya</th>
                                        <th>Informasi Legal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
									
                                        <td>
  											                 <div class="form-group">
    											                 <div class="col-lg-9">
                                    				<p>
                                        				<b>Bank</b>
                                   				 	</p>
                                    				<p>
                                             <?php echo $i->bank;?>
                                            </p>
                                    			</div>	
                                			</div>
  											              <div class="form-group">
  												                <p>
                                        			<b>Nama Pemilik</b>
                                   				</p>
    											                 <p>
                                           <?php echo $i->nama_pemilik ?>
                                            </p>
  											              </div>
  											              <div class="form-group">
  												                <p>
                                        			<b>Nomer Rekening</b>
                                   			  </p>
    											                <p>
                                            <?php echo $i->no_rekening ?>
                                          </p>
  											               </div>
  											               <div class="form-group">
  												                <p>
                                        			<b>Kantor Cabang</b>
                                   				</p>
    											                <p>
                                            <?php echo $i->kantor_cabang; ?>
                                          </p>
  											                 </div>
  											                 <div class="form-group">
  												                <p>
                                        		<b>Kota</b>
                                   				</p>
                                          <p>
                                            <?php echo $i->kota; ?>
                                          </p>
  											               </div>
                                        </td>
                                        <td>
                                        	<div class="form-group">
  												                  <p>
                                        			<b>Upload Foto KTP</b>
                                   				 </p>
    											                 <p>
                                             <img src="<?php echo base_url(); ?><?php echo $i->foto_ktp; ?>" width = "400" height="200">
                                            </p>
  											                   </div>										
                                        	<div class="form-group">
  												                <p>
                                        			<b>No KTP</b>
                                   				</p>
    											                <p>
                                            <?php echo $i->no_ktp; ?>
                                          </p>
  											                 </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                          <?php } ?>
                        </div>
                        <a href="<?php echo base_url(); ?>controller_eo/informasi_dasar"><input type="submit" class="btn btn-primary btn-user btn-block" value="Kembali" style=""></a>
                    </div>
                </div>
            </div>

