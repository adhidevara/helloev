<?php $this->load->view('pages_eo/header_eo'); ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(); ?>file/img/man.PNG">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

   
</head>

<body>
    <section class="" style="margin-left:50px">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-3">
                    <div class="card profile-card" style="background-color:lightgrey">
                        <div class="profile-header">&nbsp;</div>
                        <div class="profile-body" style="padding:50px;background-color:lightgrey">
                            <div class="image-area">
                                <img src="<?php echo base_url()?>file/img/man.PNG" alt="AdminBSB - Profile Image" style="width: 150px; height: 150px" />
                            </div>
                            <div class="content-area">
                                <h4  style="margin-left:30px"><?php echo $this->session->userdata("nama"); ?></h4>
                                <!-- <h4><b>Penyelenggara Event</b></h4> -->
                            </div>
                        </div>
                        <div class="profile-footer">
                            <ul>
                                <li>
                                    <span>Email</span><br>
                                    <span><font size="2"><?php echo $data[0]->Email; ?></font></span>
                                </li>
                                <li>
                                    <span>Alamat</span><br>
                                    <span><font size="2"><?php echo $data[0]->Alamat; ?></font></span>
                                </li>
                            </ul>
                            <br><br>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:20px">
                                    <div class="card">
                                        <div class="header">
                                            <h2><?php echo $data[0]->nama_event; ?></h2>
                                        </div>
                                        <div class="body">

                                            <div id="wizard_horizontal " style="padding:20px">
                                                <h2>Detail Event</h2>
                                                <section>
                                                    <fieldset>
                                                        <div class="panel-body">
                                                            <div class="post">
                                                                <div class="post-heading">
                                                                    <p>Cerita</p> <br>
                                                                </div>
                                                                <div class="post-content">
                                                                    <p style=";padding-left: 50px;padding-right: 50px"><?php echo $data[0]->deskripsi; ?></p>
                                                                    <br>
                                                                    <center>
                                                                    <img src="<?php echo base_url().'file/img/'.$data[0]->gambar; ?>" class="img-responsive" style="width: 80%; height: auto; border-radius: 50px" />
                                                                            </center>
                                                                            <br>
                                                                    <br><br><br>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </section>

                                                <h2>Detail Event</h2>
                                                <section>
                                                    <fieldset>

                                                        <!-- <input type="hidden" name="id_campaign" class="form-control" value="<?=$id?>" id="id_campaign"> -->

                                                        <div class="form-group form-float">
                                                            <div class="form-line">
                                                                <!-- <input type="text" name="nama" class="form-control" id="nama" value="<?php echo $this->session->userdata('nama'); ?>" required> -->
                                                                <label class="form-label">Nama Lengkap</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-float">
                                                            <div class="form-line">
                                                                <!-- <input type="email" name="email" class="form-control" id="email" value="<?php echo $this->session->userdata('email'); ?>" required> -->
                                                                <label class="form-label">Email</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-float">
                                                            <!-- <div class="form-line">
                                                                <input type="number" name="nominal" class="form-control" id="nominal" required>
                                                                <label class="form-label">Nominal Donasi (Rp.)</label>
                                                            </div> -->
                                                            <div class="form-line">
                                                                <select class="form-control">
                                                                    <option>asd</option>
                                                                    <option></option>
                                                                </select>
                                                                <input type="number" class="form-control" name="nominal" id="nominal" required="" aria-required="true" value="0">
                                                                <label class="form-label">Nominal Donasi (Rp.)</label>
                                                            </div>
                                                        </div>

                                                       

                                                    </fieldset>
                                                </section>

                                                <h2>Lakukan Pembayaran</h2>
                                                <section>
                                                    <fieldset>
                                                        <label class="form-label">METODE PEMBAYARAN</label>
                                                            <div class="form-check">
                                                                <input type="radio" class="form-check-input" id="method1" name="metode_bayar" value="Virtual Account" required="">
                                                                <label class="form-check-label" for="method1">VIRTUAL ACCOUNT</label>
                                                            </div>

                                                            <!-- Group of material radios - option 2 -->
                                                            <div class="form-check">
                                                                <input type="radio" class="form-check-input" id="method2" name="metode_bayar" value="Transfer Bank" required="">
                                                                <label class="form-check-label" for="method2">TRANSFER BANK</label>
                                                            </div>
                                                           
                                                        <input id="acceptTerms-2" name="acceptTerms" type="checkbox" required>
                                                        <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
                                                    </fieldset>
                                                </section>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url(); ?>file/dashTemplate/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(); ?>file/dashTemplate/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url(); ?>file/dashTemplate/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url(); ?>file/dashTemplate/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="<?php echo base_url(); ?>file/dashTemplate/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="<?php echo base_url(); ?>file/dashTemplate/plugins/jquery-steps/jquery.steps.js"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="<?php echo base_url(); ?>file/dashTemplate/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(); ?>file/dashTemplate/plugins/node-waves/waves.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url(); ?>file/dashTemplate/js/admin.js"></script>
    <script src="<?php echo base_url(); ?>file/dashTemplate/js/pages/examples/profile.js"></script>
    <script src="<?php echo base_url(); ?>file/dashTemplate/js/pages/forms/form-wizard.js"></script>

    <!-- Demo Js -->
    <script src="<?php echo base_url(); ?>file/dashTemplate/js/demo.js"></script>

    

</body>
</html>
<?php $this->load->view('pages_eo/footer_eo'); ?>