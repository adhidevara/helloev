<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin  - Dashboard</title>

  <!-- Custom fonts for this template -->
  <link href="<?php echo base_url() ?>file/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo base_url() ?>file/css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="<?php echo base_url() ?>file/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  
  <link rel="shortcut icon" href="<?php echo base_url() ?>file/img/icon2.png">

</head>


        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data List Penyelenggara Event</h1>
          
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">  
            <h6 class="m-0 font-weight-bold text-primary">&nbsp&nbsp&nbsp&nbspKetikan data yang ingin dicari</h6>
            <form method="post" action="<?php echo base_url() ?>controller/carievent" class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <input type="text" name="cari" class="form-control bg-light">
            <button type="submit" class="btn btn-primary">Cari</button><br>
            </form>
              <div class="card-body">
              <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        
            
        
        
              <tr>
            <th>No</th>
            <th>Penyelenggara</th>
            <th>Nama Event</th>
            <th>Tanggal Acara</th>
            <th>Waktu Mulai Acara</th>
            <th>Waktu Berakhir Acara</th>
            <th>Lokasi</th>
            <th>Harga</th>
            <th>Jumlah Ticket</th>
            <th>Tiket Belum Terjual</th>
            <th>Tiket Terjual</th>
            <th>Gambar</th>
            <th>Berkas Event</th>
            <th>Aksi</th>
            <th>Status</th>
		</tr>
		<?php 
		$no = 1;
		foreach($user as $u){ 
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $u->penyelenggara ?></td>
			<td><?php echo $u->nama_event ?></td>
			<td><?php echo $u->tgl_acara ?></td>
      <td><?php echo $u->waktu_mulai_acara ?></td>
      <td><?php echo $u->waktu_berakhir_acara ?></td>
      <td><?php echo $u->lokasi ?></td>
      <td><?php echo "Rp.".number_format($u->harga_tiket,2,",",".") ?></td>
      <td><?php echo $u->jumlah_stok_tiket ?></td>
      <td><?php echo $u->sisa ?></td>
      <td><?php echo $u->terjual ?></td>
      <td><img src="<?php echo base_url().'file/img/'.$u->gambar ?>" alt="" width="150px"></td>
      <td><img src="<?php echo base_url().$u->berkas_event ?>" alt="" width="150px"></td>
      <td>
           <a href="<?php echo base_url(); ?>controller/editevent/<?php echo $u->ID_event; ?>" class="badge badge-primary float-right"  style="padding-top: 10px;padding-right: 23px; padding-bottom: 10px;padding-left: 10px;">Edit</a>
                <a href="<?php echo base_url(); ?>controller/hapusevent/<?php echo $u->ID_event; ?>" class="badge badge-danger float-right" onclick="return confirm('Anda yakin?'); " style="padding-top: 10px;padding-right: 10px; padding-bottom: 10px;padding-left: 10px;">Delete</a>  
      </td>
      <td>
        <?php if ($u->Status=="Belum Valid") { ?>
          <a href="<?php echo base_url(); ?>controller/editstatus/<?php echo $u->ID_event; ?>" class="badge badge-primary float-right"  style="padding-top: 10px;padding-right: 23px; padding-bottom: 10px;padding-left: 10px;">Confirm</a>        
        <?php }elseif($u->Status == "Tervalidasi") { ?>
          <span class="badge badge-success"><?= $u->Status ?></span>
		  <?php }elseif($u->Status == "Canceled By Admin") { ?>
		  <span class="badge badge-danger"><?= $u->Status ?></span>
		  <?php } ?>
      </td>
		</tr>
		<?php } ?>
	</table>
  <?php 
             echo $this->pagination->create_links();
            ?>
            </div>
            </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">


      
