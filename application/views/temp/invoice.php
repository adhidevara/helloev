<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width">
	<meta name="HandheldFriendly" content="true" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!--[if gte IE 7]><html class="ie8plus" xmlns="http://www.w3.org/1999/xhtml"><![endif]-->
	<!--[if IEMobile]><html class="ie8plus" xmlns="http://www.w3.org/1999/xhtml"><![endif]-->
	<meta name="format-detection" content="telephone=no">
	<meta name="generator" content="EDMdesigner, www.edmdesigner.com">
	<title>Untitled 10 Mar 2019 23:47:37 GMT</title>

	<!--##custom-font-resource##-->

	<style type="text/css" media="screen">
		* {
			line-height: inherit;
		}

		.ExternalClass * {
			line-height: 100%;
		}

		body,
		p {
			margin: 0;
			padding: 0;
			margin-bottom: 0;
			-webkit-text-size-adjust: none;
			-ms-text-size-adjust: none;
		}

		img {
			line-height: 100%;
			outline: none;
			text-decoration: none;
			-ms-interpolation-mode: bicubic;
		}

		a img {
			border: none;
		}

		a,
		a:link,
		.no-detect-local a,
		.appleLinks a {
			color: #5555ff !important;
			text-decoration: underline;
		}

		.ExternalClass {
			display: block !important;
			width: 100%;
		}

		.ExternalClass,
		.ExternalClass p,
		.ExternalClass span,
		.ExternalClass font,
		.ExternalClass td,
		.ExternalClass div {
			line-height: inherit;
		}

		table td {
			border-collapse: collapse;
			mso-table-lspace: 0pt;
			mso-table-rspace: 0pt;
		}

		.mobile_link a[href^="tel"],
		.mobile_link a[href^="sms"] {
			text-decoration: default;
			color: #5555ff !important;
			pointer-events: auto;
			cursor: default;
		}

		.no-detect a {
			text-decoration: none;
			color: #5555ff;
			pointer-events: auto;
			cursor: default;
		}

			{
			color: #5555ff;
		}

		span {
			color: inherit;
			border-bottom: none;
		}

		span:hover {
			background-color: transparent;
		}

		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
		}

		.nounderline {
			text-decoration: none !important;
		}

		h1,
		h2,
		h3 {
			margin: 0;
			padding: 0;
		}

		p {
			Margin: 0px !important;
		}

		table[class="email-root-wrapper"] {
			width: 600px !important;
		}

		body {}

		body {
			min-width: 280px;
			width: 100%;
		}

		td[class="pattern"] .c300p50r {
			width: 50%;
		}
	</style>
	<style>
		@media only screen and (max-width: 599px),
		only screen and (max-device-width: 599px),
		only screen and (max-width: 400px),
		only screen and (max-device-width: 400px) {
			.email-root-wrapper {
				width: 100% !important;
			}

			.full-width {
				width: 100% !important;
				height: auto !important;
				text-align: center;
			}

			.fullwidthhalfleft {
				width: 100% !important;
			}

			.fullwidthhalfright {
				width: 100% !important;
			}

			.fullwidthhalfinner {
				width: 100% !important;
				margin: 0 auto !important;
				float: none !important;
				margin-left: auto !important;
				margin-right: auto !important;
				clear: both !important;
			}

			.hide {
				display: none !important;
				width: 0px !important;
				height: 0px !important;
				overflow: hidden;
			}

			.c300p50r {
				width: 100% !important;
				float: none;
			}

		}
	</style>
	<style>
		@media only screen and (min-width: 600px) {
			td[class="pattern"] .c300p50r {
				width: 300px !important;
			}

		}

		@media only screen and (max-width: 599px),
		only screen and (max-device-width: 599px),
		only screen and (max-width: 400px),
		only screen and (max-device-width: 400px) {
			table[class="email-root-wrapper"] {
				width: 100% !important;
			}

			td[class="wrap"] .full-width {
				width: 100% !important;
				height: auto !important;
			}

			td[class="wrap"] .fullwidthhalfleft {
				width: 100% !important;
			}

			td[class="wrap"] .fullwidthhalfright {
				width: 100% !important;
			}

			td[class="wrap"] .fullwidthhalfinner {
				width: 100% !important;
				margin: 0 auto !important;
				float: none !important;
				margin-left: auto !important;
				margin-right: auto !important;
				clear: both !important;
			}

			td[class="wrap"] .hide {
				display: none !important;
				width: 0px;
				height: 0px;
				overflow: hidden;
			}

			.edm-social {
				width: 100% !important;
			}

			td[class="pattern"] .c300p50r {
				width: 100% !important;
			}

		}
	</style>

	<style>
		@media screen and (min-width: 600px) {
			.dh {
				display: none;
			}
		}
	</style>
	<!--[if (gte mso 9)|(IE)]>
	<style>
		.dh {
			display: none;
		}

		.dh table {
			mso-hide: all;
		}
	</style>
<![endif]-->

	<!--[if (gte IE 7) & (vml)]>
<style type="text/css">
html, body {margin:0 !important; padding:0px !important;}
img.full-width { position: relative !important; }

.img300x77 { width: 300px !important; height: 77px !important;}

</style>
<![endif]-->

	<!--[if gte mso 9]>
<style type="text/css">
.mso-font-fix-arial { font-family: Arial, sans-serif;}
.mso-font-fix-georgia { font-family: Georgia, sans-serif;}
.mso-font-fix-tahoma { font-family: Tahoma, sans-serif;}
.mso-font-fix-times_new_roman { font-family: 'Times New Roman', sans-serif;}
.mso-font-fix-trebuchet_ms { font-family: 'Trebuchet MS', sans-serif;}
.mso-font-fix-verdana { font-family: Verdana, sans-serif;}
</style>
<![endif]-->

	<!--[if gte mso 9]>
<style type="text/css">
table, td {
border-collapse: collapse !important;
mso-table-lspace: 0px !important;
mso-table-rspace: 0px !important;
}

.email-root-wrapper { width 600px !important;}
.imglink { font-size: 0px; }
.edm_button { font-size: 0px; }
</style>
<![endif]-->

	<!--[if gte mso 15]>
<style type="text/css">
table {
font-size:0px;
mso-margin-top-alt:0px;
}

.fullwidthhalfleft {
width: 49% !important;
float:left !important;
}

.fullwidthhalfright {
width: 50% !important;
float:right !important;
}
</style>
<![endif]-->
	<STYLE type="text/css" media="(pointer) and (min-color-index:0)">
		html,
		body {
			background-image: none !important;
			background-color: transparent !important;
			margin: 0 !important;
			padding: 0 !important;
		}
	</STYLE>

</head>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="font-family:Arial, sans-serif; font-size:0px;margin:0;padding:0;">
	<!--[if t]><![endif]-->
	<!--[if t]><![endif]-->
	<!--[if t]><![endif]-->
	<!--[if t]><![endif]-->
	<!--[if t]><![endif]-->
	<!--[if t]><![endif]-->
	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="margin:0; padding:0; width:100% !important;">
		<tr>
			<td class="wrap" align="center" valign="top" width="100%">
				<center>
					<!-- content -->
					<div style="padding:0px">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td valign="top" style="padding:0px">
									<table cellpadding="0" cellspacing="0" width="600" align="center" style="max-width:600px;min-width:240px;margin:0 auto" class="email-root-wrapper">
										<tr>
											<td valign="top" style="padding:0px" class="pattern">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td valign="top" style="padding:0;mso-cellspacing:0in">
															<table cellpadding="0" cellspacing="0" border="0" align="left" width="300" id="c300p50r" style="float:left" class="c300p50r">
																<tr>
																	<td valign="top" style="padding:0px">
																		<table cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td align="center" style="padding:0px">
																					<table cellpadding="0" cellspacing="0" border="0" align="center" width="300" height="77" style="border:0px none;height:auto" class="full-width">
																						<tr>
																							<td valign="top" style="padding:0px"><img src="https://images.chamaileon.io/L1.png" width="300" height="77" alt="" border="0" style="display:block;width:100%;height:auto" class="full-width img300x77" /></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!--[if gte mso 9]></td><td valign="top" style="padding:0;"><![endif]-->
															<table cellpadding="0" cellspacing="0" border="0" align="left" width="300" id="c300p50r" style="float:left" class="c300p50r">
																<tr>
																	<td valign="top" style="padding:0px">
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
																			<tr>
																				<td valign="top" style="padding:10px">
																					<div style="text-align:left;font-family:Arial;font-size:14px;color:#000000;line-height:22px;mso-line-height:exactly;mso-text-raise:4px">
																						<p style="padding: 0; margin: 0;text-align: right;">KETRINGAN &nbsp;<br>Jalan Telekomunikasi No.1 &nbsp;<br>Bandung, Jawa Barat &nbsp;<br>082291865050 &nbsp;</p>
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>

												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td valign="top" style="padding:0px">
									<table cellpadding="0" cellspacing="0" width="600" align="center" style="max-width:600px;min-width:240px;margin:0 auto" class="email-root-wrapper">
										<tr>
											<td valign="top" style="padding:0px">
												<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border:0px none">
													<tr>
														<td valign="top" style="padding:0px">
															<table cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td style="padding:0px" class="pattern">
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
																			<tr>
																				<td valign="top" style="padding:10px">
																					<table cellpadding="0" cellspacing="0" width="100%">
																						<tr>
																							<td style="padding:0px">
																								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-top:2px solid #a9a9a9">
																									<tr>
																										<td valign="top">
																											<table cellpadding="0" cellspacing="0" width="100%">
																												<tr>
																													<td style="padding:0px"></td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
																			<tr>
																				<td valign="top" style="padding:10px">
																					<div style="text-align:left;font-family:Arial;font-size:14px;color:#A72F4A;line-height:22px;mso-line-height:exactly;mso-text-raise:4px">
																						<p style="padding: 0; margin: 0;"><span style="font-size:48px;"><strong>INVOICE</strong></span></p>
																					</div>
																				</td>
																			</tr>
																		</table>
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
																			<tr>
																				<td valign="top" style="padding:10px">
																					<div style="text-align:left;font-family:Arial;font-size:14px;color:#000000;line-height:22px;mso-line-height:exactly;mso-text-raise:4px">
																						<p style="padding: 0; margin: 0;"><strong>KODE PESANAN : <?php echo $detail_pesanan->Id_Pesanan; ?> &nbsp;</strong><br> <?php echo $detail_pesanan->Nama_Konsumen; ?></p>
																					</div>
																				</td>
																			</tr>
																		</table>
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
																			<tr>
																				<td valign="top" style="padding:10px">
																					<table cellpadding="0" cellspacing="0" width="100%">
																						<tr>
																							<td style="padding:0px">
																								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-top:2px solid #a9a9a9">
																									<tr>
																										<td valign="top">
																											<table cellpadding="0" cellspacing="0" width="100%">
																												<tr>
																													<td style="padding:0px"></td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>

																		<!--=== MULAINYA TABEL-->
																		<center>
																			<table border="0">
																				<tr style="background: #A72F4A">
																					<th>

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#FFFFFF;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;"><strong>No.</strong></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</th>
																					<th>

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#FFFFFF;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;"><strong>Produk</strong></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</th>
																					<th>

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#FFFFFF;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;"><strong>Harga</strong></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</th>
																					<th>

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#FFFFFF;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;"><strong>Jumlah</strong></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</th>
																					<th>

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#FFFFFF;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;"><strong>Total</strong></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</th>
																				</tr>
																<?php $total = 0; $nomor = 1; ?>
																<?php foreach ($menu_dipesan as $key => $value): ?>
																				<tr>
																					<td>

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">

																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#3D3D3D;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;"><?php echo $nomor; ?></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</td>
																					<td>

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">

																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#3D3D3D;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;"><?php echo $value->Nama_Paket ?></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</td>
																					<td>

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#3D3D3D;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;">Rp <?php echo number_format($value->Harga_Paket) ?></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</td>
																					<td>

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#3D3D3D;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;"><?php echo $value->Jumlah_Kotak ?></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</td>
																					<td>

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#3D3D3D;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;">Rp <?php echo number_format($value->Harga_Paket*$value->Jumlah_Kotak) ?></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</td>
																				</tr>
																				<?php $nomor++; endforeach; ?>
																				<tr style="background: #C9C9C9">
																					<td colspan="4">

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#3D3D3D;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;"><strong>Sub Total</strong></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</td>
																					<td>

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#3D3D3D;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;">Rp <?php echo number_format($detail_pesanan->Tagihan); ?></p>
																									</div>
																								</td>
																							</tr>

																						</table>

																					</td>

																				</tr>
																				<tr style="background: #C9C9C9">

																					<td colspan="4">

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#3D3D3D;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;"><strong>Kode Pembayaran</strong></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</td>
																					<td>

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#3D3D3D;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;">- Rp<?php echo $detail_pesanan->Kode_Unik; ?></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</td>

																				</tr>
																				<tr style="background: #C9C9C9">

																					<td colspan="4">

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#3D3D3D;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;"><strong>Total Bayar</strong></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</td>
																					<td>

																						<table cellpadding="0" cellspacing="0" border="0" width="100%">
																							<tr>
																								<td valign="top" style="padding:10px">
																									<div style="text-align:left;font-family:Roboto, Helvetica Neue, Helvetica, Arial, sans-serif;font-size:15px;color:#3D3D3D;line-height:26px;mso-line-height:exactly;mso-text-raise:5px">
																										<p style="padding: 0; margin: 0; text-align: center;"><strong>Rp <?php echo number_format($detail_pesanan->Total_Tagihan) ?> </strong></p>
																									</div>
																								</td>
																							</tr>
																						</table>

																					</td>

																				</tr>
																			</table>
																		</center>
																		<!--===-->
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
																			<tr>
																				<td valign="top" style="padding:10px">
																					<table cellpadding="0" cellspacing="0" width="100%">
																						<tr>
																							<td style="padding:0px">
																								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-top:2px solid #a9a9a9">
																									<tr>
																										<td valign="top">
																											<table cellpadding="0" cellspacing="0" width="100%">
																												<tr>
																													<td style="padding:0px"></td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
																			<tr>
																				<td valign="top" style="padding:0;mso-cellspacing:0in">
																					<table cellpadding="0" cellspacing="0" border="0" align="left" width="300" id="c300p50r" style="float:left" class="c300p50r">
																						<tr>
																							<td valign="top" style="padding:0px">
																								<table cellpadding="0" cellspacing="0" border="0" width="100%">
																									<tr>
																										<td valign="top" style="padding:10px">
																											<div style="text-align:left;font-family:Arial;font-size:14px;color:#000000;line-height:22px;mso-line-height:exactly;mso-text-raise:4px">
																												<p style="padding: 0; margin: 0;"><strong>Detail Pemesanan &nbsp;</strong><br>Tanggal Pemesanan : &nbsp;<br><?php echo date("d - m - Y", strtotime($detail_pesanan->Tanggal_Pesan)); ?>&nbsp;<br>Tanggal Pengiriman Pesanan : &nbsp;<br><?php echo date("d - m - Y", strtotime($detail_pesanan->Tanggal_Kegiatan)); ?>
																												<br>Alamat Pengiriman : <br><?php echo $detail_pesanan->Alamat_Pengiriman; ?>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																					<!--[if gte mso 9]></td><td valign="top" style="padding:0;"><![endif]-->
																					<table cellpadding="0" cellspacing="0" border="0" align="left" width="300" id="c300p50r" style="float:left" class="c300p50r">
																						<tr>
																							<td valign="top" style="padding:0px">
																								<table cellpadding="0" cellspacing="0" border="0" width="100%">
																									<tr>
																										<td valign="top" style="padding:10px">
																											<div style="text-align:left;font-family:Arial;font-size:14px;color:#000000;line-height:22px;mso-line-height:exactly;mso-text-raise:4px">
																												<p style="padding: 0; margin: 0;text-align: right;"><strong>Detail Invoice &nbsp;</strong><br>Tanggal Invoice : &nbsp;<br><?php echo date("d - m - Y"); ?> &nbsp;<br>Waktu Invoice : &nbsp;<br><?php echo date("H : i"); ?> &nbsp;</p>
																											</div>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td valign="top" style="padding:0px">
									<table cellpadding="0" cellspacing="0" width="600" align="center" style="max-width:600px;min-width:240px;margin:0 auto" class="email-root-wrapper">
										<tr>
											<td valign="top" style="padding:0px">
												<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border:0px none">
													<tr>
														<td valign="top" style="padding:0px">
															<table cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td style="padding:0px">
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
																			<tr>
																				<td valign="top" style="padding:10px">
																					<table cellpadding="0" cellspacing="0" width="100%">
																						<tr>
																							<td style="padding:0px">
																								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-top:2px solid #a9a9a9">
																									<tr>
																										<td valign="top">
																											<table cellpadding="0" cellspacing="0" width="100%">
																												<tr>
																													<td style="padding:0px"></td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
																			<tr>
																				<td valign="top" style="padding:10px">
																					<div style="text-align:left;font-family:Arial;font-size:14px;color:#000000;line-height:22px;mso-line-height:exactly;mso-text-raise:4px">
																						<p style="padding: 0; margin: 0;"><b>STATUS</b>:</p>
																					</div>
																				</td>
																			</tr>
																		</table>
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
																			<tr>
																				<td valign="top" style="padding:10px">
																					<div style="text-align:left;font-family:Arial;font-size:14px;color:#1AC600;line-height:22px;mso-line-height:exactly;mso-text-raise:4px">
																						<p style="padding: 0; margin: 0;"><strong><span style="font-size:36px;">LUNAS</span></strong></p>
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
					<!-- content end -->
				</center>
			</td>
		</tr>
	</table>
</body>

</html>
