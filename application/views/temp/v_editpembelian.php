 <!DOCTYPE html>
<html>
<head>
	
</head>
<body>
	
	<?php foreach($user as $u){ ?>
	<form action="<?php echo base_url(). 'controller/updatepembeli'; ?>" method="post">
		<table style="margin:20px auto;">
			<tr>
				<td>Nama Pembeli</td>
				<td><input type="text" class="form-control form-control-user" name="lokasi" value="<?php echo $u->username ?>" readonly></td>
			</tr>
			<tr>
				<td>Total</td>
				<td><input type="text" class="form-control form-control-user" name="lokasi" value="<?php echo $u->subtotal ?>" readonly></td>
			</tr>
			<tr>
				<td>Nama Event</td>
				<td>
					<input type="hidden" name="ID_pembeli" value="<?php echo $u->ID_pembeli ?>">
					<input type="text" class="form-control form-control-user" name="nama_event" value="<?php echo $u->nama_event; ?>" readonly>
				</td>
			</tr>
			<tr>
				<td>Lokasi Event</td>
				<td><input type="text" class="form-control form-control-user" name="lokasi" value="<?php echo $u->lokasi ?>" readonly></td>
			</tr>
			<tr>
				<td>Tangga Event</td>
				<td><input type="text" class="form-control form-control-user"  name="tgl_acara" value="<?php echo $u->tgl_acara ?>" readonly></td>
            </tr>

            <tr>
				<td>Status Pembayaran</td>
				<td><select name="status_pembayaran">
                <option value="Refund" <?php if ($u->status_pembayaran == 'Refund') {
                    echo "selected";
                }?>>Refund</option>
                <option value="On Progres" <?php if ($u->status_pembayaran == 'On Progres') {
                    echo "selected";
                }?>>On Progres</option>

                <option value="Success" <?php if ($u->status_pembayaran == 'Success') {
                    echo "selected";
                }?>>Success</option>
                </select></td>
            </tr>
				<td></td>
				<td><input type="submit" class="btn btn-primary btn-user btn-block"	 name="submit" value="Simpan"></td>
			</tr>
		</table>
	</form>	
	<?php } ?>
</body>
</html>