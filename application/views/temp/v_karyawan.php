<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin  - Dashboard</title>

  <!-- Custom fonts for this template -->
  <link href="<?php echo base_url() ?>file/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo base_url() ?>file/css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="<?php echo base_url() ?>file/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  
  <link rel="shortcut icon" href="<?php echo base_url() ?>file/img/icon2.png">

</head>


        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data List Admin</h1>
          
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">  
            <h6 class="m-0 font-weight-bold text-primary">&nbsp&nbsp&nbsp&nbspKetikan data yang ingin dicari</h6>
            <form method="post" action="<?php echo base_url() ?>controller/cari" class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <input type="text" name="cari" class="form-control bg-light">
            <button type="submit" class="btn btn-primary">Cari</button><br>
            </form>
              <div class="card-body">
              <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <tr>
			<th>No</th>
			<th>Nama</th>
			<th>Email</th>
			<th>Password</th>
      <th>Alamat</th>
      <th>Aksi</th>
		</tr>
		<?php 
		$no = 1;
		foreach($user as $u){ 
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $u->Nama ?></td>
			<td><?php echo $u->Email ?></td>
			<td><?php echo $u->Password ?></td>
            <td><?php echo $u->Alamat ?></td>
            <td>
			    <!-- <?php echo anchor('controller/edit/'.$u->ID,'Edit'); ?> -->
          <a href="<?php echo base_url(); ?>controller/edit/<?php echo $u->ID; ?>" class="badge badge-primary float-right"  style="padding-top: 10px;padding-right: 23px; padding-bottom: 10px;padding-left: 10px;">Edit</a>
                <!-- <?php echo anchor('controller/hapus/'.$u->ID,'Hapus'); ?> -->
                <a href="<?php echo base_url(); ?>controller/hapus/<?php echo $u->ID; ?>" class="badge badge-danger float-right" onclick="return confirm('Anda yakin?'); " style="padding-top: 10px;padding-right: 10px; padding-bottom: 10px;padding-left: 10px;">Delete</a> 
			</td>
		</tr>
		<?php } ?>
	</table>
  <?php echo $this->pagination->create_links(); ?>
            </div>
            </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">


      
