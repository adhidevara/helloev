<?php
            $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
            $pdf->SetTitle('INVOICE EVENT');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->AddPage();
            $i=0;
            $html='<center><p style="font-size:20px ; text-align:center;">INVOICE PEMBELI</p></center>
                    <table cellspacing="1" bgcolor="#666666" cellpadding="1">
                        <tr bgcolor="#ffffff">
                            <th align="center">No</th>
                            <th align="center">Purchase Date</th>
                            <th align="center">Event Name</th>
                            <th align="center">Event Location</th>
                            <th align="center">Event Schedule</th>
                            <th align="center">Subtotal</th>
                            <th align="center">Event Status</th>
                            
                        </tr>';
            foreach ($tiket as $row) 
                {
                    $i++;
                    $html.='<tr bgcolor="#ffffff">
                            <td align="center">'.$i.'</td>
                            <td>'.$row['tanggal_dibeli'].'</td>
                            <td>'.$row['nama_event'].'</td>
                            <td>'.$row['lokasi'].'</td>
                            <td>'.$row['tgl_acara'].'</td>
                            <td>'.$row['subtotal'].'</td>
                            <td>'.$row['status_pembayaran'].'</td>
                        </tr>';
                }
            $html.='</table>';
            $pdf->writeHTML($html, true, false, true, false, '');
            $pdf->Output('invoice.pdf', 'I');
?>