<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin  - Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url() ?>file/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url() ?>file/css/sb-admin-2.min.css" rel="stylesheet">
  <link rel="shortcut icon" href="<?php echo base_url() ?>file/img/icon2.png">
  

</head>

<body class="bg-gradient-primary">


  <div class="container">
    <div class = "container col-lg-7  ">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-1">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!-- <div class="col-lg-4 d-none d-lg-block "></div> -->
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
              </div>
              <form class="user" action="<?php echo base_url()?>controller/tambah_aksi" method="POST">
              <!-- <?php echo form_open_multipart('controller/tambah_aksi');?> -->
                <div class="form-group ">
                  <!-- <div class="col-sm-6 mb-3 mb-sm-0"> -->
                    <input type="text" name="Nama" class="form-control form-control-user" id="exampleFirstName" placeholder="First Name">
                  <?php
                  if (form_error('Nama')){
                  ?>
                  <br>
                  <div class="alert-danger"></div>
                  <?php
                  echo form_error('Nama');
                  echo "</div>";
                  }
                  ?>
                  </div>
                  <!-- <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" id="exampleLastName" placeholder="Last Name">
                  </div> -->
                <!-- </div> -->
                <div class="form-group">
                  <input type="email" name="Email" class="form-control form-control-user" id="exampleInputEmail" placeholder="Email Address">
                  <?php
                  if (form_error('Email')){
                  ?>
                  <br>
                  <div class="alert-danger"></div>
                  <?php
                  echo form_error('Email');
                  echo "</div>";
                  }
                  ?>
                </div>
                <div class="form-group ">
                  <!-- <div class="col-sm-6 mb-3 mb-sm-0"> -->
                    <input type="password" name="Password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                    <?php
                  if (form_error('Password')){
                  ?>
                  <br>
                  <div class="alert-danger"></div>
                  <?php
                  echo form_error('Password');
                  echo "</div>";
                  }
                  ?>
                  </div>
                  <!-- <div class="col-sm-6">
                    <input type="password" name="Password" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Repeat Password">
                  </div> -->
                <!-- </div> -->
                <div class="form-group">
                  <input type="textarea" name="Alamat" class="form-control form-control-user" id="exampleInputEmail" placeholder="Address">
                  <?php
                  if (form_error('Alamat')){
                  ?>
                  <br>
                  <div class="alert-danger"></div>
                  <?php
                  echo form_error('Alamat');
                  echo "</div>";
                  }
                  ?>
                </div>


                <!-- <div class="form-group">
                  <input type="file" name="foto " class="form-control form-control-user" id="exampleInputEmail" placeholder="Address">
                </div> -->

                
                <input type="submit" name="submit" value="Register Account" class="btn btn-primary btn-user btn-block">
                <hr>
                <!-- <a href="<?php echo base_url()?>controller/registrasi" class="btn btn-google btn-user btn-block">
                  <i class="fab fa-google fa-fw"></i> Register with Google
                </a>
                <a href="<?php echo base_url()?>controller/registrasi" class="btn btn-facebook btn-user btn-block">
                  <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                </a> -->
              </form>
              <hr>
              <div class="text-center">
                <!-- <a class="small" href="<?php echo base_url()?>controller/registrasi">Forgot Password?</a> -->
              </div>
              <div class="text-center">
                <a class="small" href="<?php echo base_url()?>controller/awal">Already have an account? Login!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url() ?>file/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>file/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url() ?>file/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url() ?>file/js/sb-admin-2.min.js"></script>

</body>

</html>
