<?php

            $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
            $pdf->SetTitle('INVOICE EVENT');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->AddPage();
            $i=0;
            
            $html=' <center>
                        <p style="font-size:20px ; text-align:center;">REPORT DATA PEMBELI</p>
                    </center>
                    <table style="">
                        <tr>
                        <td></td>
                        </tr>
                        <tr>
                        <td></td>
                        <td></td>
                        </tr>
                    </table>
                    <table cellspacing="1" bgcolor="#666666" cellpadding="4">
                        <tr bgcolor="#ffffff" >
                            <th  align="center" >No</th>
                            <th align="center">Tanggal Dibeli</th>
                            <th  align="center">Nama Pembeli</th>
                            <th  align="center">Harga</th>
                            <th  align="center">Jumlah</th>
                            <th  align="center">Subtotal</th>
                        </tr>';
            foreach ($event as $row) 
                {
                    $i++;
                    $html.='<tr bgcolor="#ffffff">
                            <td align="center">'.$i.'</td>
                            <td>'.$row['tanggal_dibeli'].'</td>
                            <td>'.$row['username'].'</td>
                            <td>'.$row['harga'].'</td>
                            <td>'.$row['jumlah_tiket'].'</td>
                            <td align="right">'.number_format($row['subtotal'],0,",",",").'</td>
                        </tr>';
                }
            $html.='</table>';
            $pdf->writeHTML($html, true, false, true, false, '');
            $pdf->Output('invoice.pdf', 'I');
?>
