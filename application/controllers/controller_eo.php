<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class controller_eo extends CI_Controller {

    function __construct(){
		parent::__construct();		
        $this->load->model('m_model_eo');
        $this->load->library('pagination');
        $this->load->helper(array('form','url'));
        $this->load->library('Ciqrcode');
        $this->load->library('Pdf');
        $this->load->library('form_validation');
        
        // $this->load->model('model_event');
        
        
		// $this->load->helper('url');
 
    }
    // ========================================Pagination Karyawan=============================================================================
    public function page()
    {
        $this->load->view('pages/header');
        
        $config['base_url'] = 'http://localhost/Heloev2/controller/page';
        $config['total_rows'] = $this->m_model_eo->tampil_data()->num_rows();
        $config['per_page'] = 2;

        $this->pagination->initialize($config);
        if ($this->uri->segment(3) != null){
            $halaman = $this->uri->segment(3);
        }else{
            $halaman = 0;
        }

        $data['user'] = $this->m_model_eo->tampildata($halaman,$config['per_page'])->result();
        $data['halaman'] = $this->pagination->create_links();
        $this->load->view('temp/v_karyawan', $data);
        $this->load->view('pages/footer');
        
        
    }
    
    // ========================================Pagination Penyelenggara=============================================================================
    
    public function page2()
    {
        $this->load->view('pages_eo/header_eo');
        
        $config['base_url'] = 'http://localhost/Heloev2/controller_eo/page2';
        $config['total_rows'] = $this->m_model_eo->tampil_data_event()->num_rows();
        $config['per_page'] = 3;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li class="page-item page-link">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="page-item page-link">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li class="page-item page-link">';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li class="page-item page-link">';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li class="page-item page-link">';
        $config['num_tag_close']    = '</li>';

        $this->pagination->initialize($config);
        if ($this->uri->segment(3) != null){
            $halaman = $this->uri->segment(3);
        }else{
            $halaman = 0;
        }
        $nama = $this->session->userdata("nama");
        $data['user'] = $this->m_model_eo->sisakuota($nama,$halaman,$config['per_page'])->result();
        $data['halaman'] = $this->pagination->create_links();
        $this->load->view('temp_eo/v_penyelenggara_eo', $data);
        $this->load->view('pages_eo/footer_eo');
        
        
    }

    
// ========================================Pagination Pembelian=============================================================================
    
public function page4()
{
    $this->load->view('pages_eo/header_eo');
    
    $config['base_url'] = 'http://localhost/Heloev2/controller_eo/page4';
    $config['total_rows'] = $this->m_model_eo->tampil_data_pembeli()->num_rows();
    $config['per_page'] = 3;
    $config['full_tag_open']    = '<ul class="pagination">';
    $config['full_tag_close']   = '</ul>';
    $config['first_link']       = 'First';
    $config['last_link']        = 'Last';
    $config['first_tag_open']   = '<li class="page-item page-link">';
    $config['first_tag_close']  = '</li>';
    $config['prev_link']        = '&laquo';
    $config['prev_tag_open']    = '<li class="page-item page-link">';
    $config['prev_tag_close']   = '</li>';
    $config['next_link']        = '&raquo';
    $config['next_tag_open']    = '<li class="page-item page-link">';
    $config['next_tag_close']   = '</li>';
    $config['last_tag_open']    = '<li class="page-item page-link">';
    $config['last_tag_close']   = '</li>';
    $config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
    $config['cur_tag_close']    = '</a></li>';
    $config['num_tag_open']     = '<li class="page-item page-link">';
    $config['num_tag_close']    = '</li>';

    $this->pagination->initialize($config);
    if ($this->uri->segment(3) != null){
        $halaman = $this->uri->segment(3);
    }else{
        $halaman = 0;
    }
    $nama = $this->session->userdata("nama");
    $data['user']= $this->m_model_eo->tampildatapembeli($nama,$halaman,$config['per_page'])->result();
    $data['halaman'] = $this->pagination->create_links();
    $this->load->view('temp_eo/v_pembelian_eo', $data);
    $this->load->view('pages_eo/footer_eo');
}

// ======================================= DETAIL EVENT ====================================

public function detail_event($id)
{
    $data['data'] = $this->m_model_eo->get_detail($id)->result();
    
    if(isset($_POST['submit'])){
        error_reporting(0);
        
        $tgl = $this->input->post();
        $data['data'] = $this->m_model_eo->get_detail_date($id, $tgl['dari'], $tgl['sampai'])->result();
        $data['info'] = "Data dari tanggal ".$tgl['dari']." sampai dengan ".$tgl['sampai']." ".count($data['data'])." Records Found";
    }

    $this->load->view('pages_eo/header_eo');
    $this->load->view('temp_eo/v_detailEvent', $data);
    $this->load->view('pages_eo/footer_eo');

}

    public function awal(){
        $this->load->view('temp/v_login');
    }

    public function menu()
    {
        $data['data_event'] = $this->m_model_eo->CountEvent()->result();
        // print_r($data);
        $data['data_event_valid'] = $this->m_model_eo->CountEventValid();
        $data['data_event_belum_valid'] = $this->m_model_eo->CountEventBelumValid();

        $data['data_user'] = $this->m_model_eo->CountUser();
        $data['data_user_berhasil'] = $this->m_model_eo->CountUserBerhasil();
        $data['data_user_belum_berhasil'] = $this->m_model_eo->CountUserBelumBerhasil();

        $data_admin = $this->session->all_userdata();
        // echo "<pre>";
        // print_r ($data_admin);
        // echo "</pre>";

        $this->load->view('pages_eo/header_eo');
        $this->load->view('temp_eo/v_dashboard_eo',$data);
        $this->load->view('pages_eo/footer_eo');
        
    }
    public function dashboard()
    {
        $data['data_event'] = $this->m_model_eo->CountEvent()->result();
        $data['data_event_valid'] = $this->m_model_eo->CountEventValid();
        $data['data_event_belum_valid'] = $this->m_model_eo->CountEventBelumValid();

        $data['data_user'] = $this->m_model_eo->CountUser();
        $data['data_user_berhasil'] = $this->m_model_eo->CountUserBerhasil();
        $data['data_user_belum_berhasil'] = $this->m_model_eo->CountUserBelumBerhasil();

        $this->load->view('pages_eo/header_eo');
        $this->load->view('temp_eo/v_dashboard_eo',$data);
        $this->load->view('pages_eo/footer_eo');    
          
    }

    
    public function detail_eo()
    {
    
        $nama = $this->session->userdata("nama");
        $data['id']= $nama;
        $data['data']= $this->m_model_eo->viewdetail_eo($nama);
        
        // echo "<pre>";
        // print_r ($data);
        // echo "</pre>";
        // $data['user'] = $this->m_model_eo->tampildataevent($nama,$halaman,$config['per_page'])->result();
        $this->load->view('temp_eo/V_detail_eo',$data);
        
        
    }

    public function ubah_password(){
        $data_penyelenggara = $this->session->all_userdata();
        $data['penyelenggara'] = $this->m_model_eo->data_penyelenggara($data_penyelenggara['ID']);
         $this->load->view('pages_eo/header_eo');
        $this->load->view('temp_eo/v_forgetpass',$data);
        $this->load->view('pages_eo/footer_eo');    
    }

    public function update_password(){
        $data_form = $this->input->post();

        if($data_form['password_baru'] != $data_form['konfirmasi_password']){
            $this->session->set_flashdata('error', 'Password Baru dan Konfirmasi Password Harus Sama!');
            redirect('controller_eo/ubah_password');
        }else{
            $data_password = array('Nama' => $data_form['Nama'] ,
                                    'Email' => $data_form['Email'],
                                    'Password' => md5($data_form['password_baru']) 
                                );
            $this->m_model_eo->update_password($data_form['id'],$data_password);
            redirect('controller_eo/ubah_password');
        }


    }

    public function informasi_dasar(){
         $this->load->view('pages_eo/header_eo');
        $this->load->view('temp_eo/v_informasiDasar2');
        $this->load->view('pages_eo/footer_eo');    
    }
    public function tiketsaya(){
        $this->load->view('pages_eo/header_eo');
       $this->load->view('temp_eo/v_tiketsaya');
       $this->load->view('pages_eo/footer_eo');
   }


    public function detail_informasi(){
        $data_penyelenggara = $this->session->all_userdata();
        $data['informasi'] = $this->m_model_eo->informasi_dasar($data_penyelenggara['ID']);
        $this->load->view('pages_eo/header_eo');
        $this->load->view('temp_eo/v_detailInformasiLegal',$data);
        $this->load->view('pages_eo/footer_eo');    
    }

    public function isi_informasi_dasar(){
         $this->load->view('pages_eo/header_eo');
        $this->load->view('temp_eo/v_informasiDasar');
        $this->load->view('pages_eo/footer_eo');    
    }

    public function update_informasiDasar(){
        $data_penyelenggara = $this->session->all_userdata();

        $data_form = $this->input->post();
        
        
        $config['upload_path'] = './foto_ktp/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']  = '100000';
        $config['max_width']  = '20000';
        $config['max_height']  = '2000';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $this->load->library('upload', $config_berkas);
        $this->upload->initialize($config_berkas);
        
        if ( !$this->upload->do_upload('fotoKTP') && !$this->upload->do_upload('fotoBerkas')){
            $error = array('error' => $this->upload->display_errors());
            echo "<pre>";
            print_r ($error);
            echo "</pre>";
        }
        else{
            $data = $this->upload->data();
            $file_ktp = 'foto_ktp/'.$data['file_name'];
            
            $informasi_dasar = array(   
                                            'bank'          => $data_form['bank'],
                                            'nama_pemilik'  => $data_form['namaPemilik'],
                                            'no_rekening'   => $data_form['nomerRekening'],
                                            'kantor_cabang' => $data_form['kantorCabang'],
                                            'kota'          => $data_form['kota'],
                                            'no_ktp'        => $data_form['noKTP'],
                                            'foto_ktp'      => $file_ktp);

            $this->m_model_eo->update_informasiDasar($data_penyelenggara['ID'],$informasi_dasar);
            redirect('controller_eo/dashboard');
        }
    }


    function editevent_eo($ID_event){
        $this->load->view('pages_eo/header_eo');
		$where = array('tiket.ID_event' => $ID_event);
        $data['user'] = $this->m_model_eo->edit_data_eo($where,'registrasievent')->result();
		$this->load->view('temp_eo/v_editevent_eo',$data);
        $this->load->view('pages_eo/footer_eo');
    }

    public function updateevent_eo()
     {
        $ID_event = $this->input->post('ID_event');
        $penyelenggara = $this->input->post('penyelenggara');
        $tgl_acara = $this->input->post('tgl_acara');
        $waktu_mulai_acara = $this->input->post('waktu_mulai_acara');
        $waktu_berakhir_acara = $this->input->post('waktu_berakhir_acara');
        $lokasi = $this->input->post('lokasi');
        
        $harga = $this->input->post('harga');
        $stok_tiket = $this->input->post('stok_tiket');
        
        
        $config_berkas['upload_path'] = './berkas_event/';
        $config_berkas['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
        $config_berkas['max_size']  = '100000';
        $config_berkas['max_width']  = '20000';
        $config_berkas['max_height']  = '2000';
        
        $this->load->library('upload', $config_berkas);
        $this->upload->initialize($config_berkas);
        
        if (!$this->upload->do_upload('berkas_event')){
            $error = array('error' => $this->upload->display_errors());
            echo "<pre>";
            print_r ($error);
            echo "</pre>";
        }else{

            $data_berkas = $this->upload->data();
            $file_berkas = 'berkas_event/'.$data_berkas['file_name'];

            $data = array(
                'ID_event' => $ID_event,
                'penyelenggara' => $penyelenggara,
                'tgl_acara' => $tgl_acara,
                'waktu_mulai_acara' => $waktu_mulai_acara,
                'waktu_berakhir_acara' => $waktu_berakhir_acara,
                'lokasi' => $lokasi,
                'berkas_event'  => $file_berkas
            );
            
            $datax = array(
                'harga' => $harga,
                'stok_tiket' => $stok_tiket
            );
            $this->m_model_eo->updateeventby_eo($ID_event,$data);
            $this->m_model_eo->updatetiketby_eo($ID_event,$datax);

            redirect('controller_eo/page2');
        }
    }
     
    
     // ====================================== Pembatalan Event ====================================

     public function vPembatalanEvent($ID_event)
     {
        $this->load->view('pages_eo/header_eo');
		$where = array('tiket.ID_event' => $ID_event);
        $data['user'] = $this->m_model_eo->edit_data_eo($where,'registrasievent')->result();
		$this->load->view('temp_eo/v_pembatalanEvent',$data);
        $this->load->view('pages_eo/footer_eo');
     }

     public function proPembatalanEvent()
     {
        $form = $this->input->post();

          
        $config_berkas['upload_path'] = './berkas_pembatalan/';
        $config_berkas['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
        $config_berkas['max_size']  = '100000';
        $config_berkas['max_width']  = '20000';
        $config_berkas['max_height']  = '2000';
        
        $this->load->library('upload', $config_berkas);
        $this->upload->initialize($config_berkas);
        
        if (!$this->upload->do_upload('berkas_pembatalan')){
            $error = array('error' => $this->upload->display_errors());
            echo "<pre>";
            print_r ($error);
            echo "</pre>";
        }
        else{
        
            $data_pembatalan = $this->upload->data();
            $file_pembatalan = 'berkas_pembatalan/'.$data_pembatalan['file_name'];

        $data = array(
            'alasan' => $form['alasan'],
            'berkas_pembatalan' => $file_pembatalan,
            'Status' => 'Pengajuan Pembatalan'
        );

        $this->m_model_eo->updatePembatalan($data, $form['ID_event']);
        redirect(base_url()."controller_eo/page2");
     }
    }

}

/* End of file Controllername.php */


?>