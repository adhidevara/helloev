<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class controller extends CI_Controller {

    function __construct(){
		parent::__construct();		
        $this->load->model('m_model');
        $this->load->model('m_model_eo');
        $this->load->library('pagination');
        $this->load->helper(array('form','url'));
        $this->load->library('Ciqrcode');
        $this->load->library('Pdf');
        $this->load->library('form_validation');
        
        // $this->load->model('model_event');
        
        
		// $this->load->helper('url');
 
    }
    // ========================================Pagination Karyawan=============================================================================
    public function page()
    {
        $this->load->view('pages/header');
        
        $config['base_url'] = 'http://localhost/Heloev2/controller/page';
        $config['total_rows'] = $this->m_model->tampil_data()->num_rows();
        $config['per_page'] = 2;

        $this->pagination->initialize($config);
        if ($this->uri->segment(3) != null){
            $halaman = $this->uri->segment(3);
        }else{
            $halaman = 0;
        }

        $data['user'] = $this->m_model->tampildata($halaman,$config['per_page'])->result();
        $data['halaman'] = $this->pagination->create_links();
        $this->load->view('temp/v_karyawan', $data);
        $this->load->view('pages/footer');
        
        
    }
    
    // ========================================Pagination Penyelenggara=============================================================================
    
    public function page2()
    {
        $this->load->view('pages/header');
        
        $config['base_url'] = 'http://localhost/Heloev2/controller/page2';
        $config['total_rows'] = $this->m_model->tampil_data_event()->num_rows();
        $config['per_page'] = 3;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li class="page-item page-link">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="page-item page-link">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li class="page-item page-link">';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li class="page-item page-link">';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li class="page-item page-link">';
        $config['num_tag_close']    = '</li>';

        $this->pagination->initialize($config);
        if ($this->uri->segment(3) != null){
            $halaman = $this->uri->segment(3);
        }else{
            $halaman = 0;
        }

        $data['user'] = $this->m_model->tampildataevent($halaman,$config['per_page'])->result();
        
        $data['halaman'] = $this->pagination->create_links();
        $this->load->view('temp/v_penyelenggara', $data);
        $this->load->view('pages/footer');
        
        
    }

    // ========================================Pagination user=============================================================================
    
    public function page3()
    {
        $this->load->view('pages/header');
        
        $config['base_url'] = 'http://localhost/Heloev2/controller/page3';
        $config['total_rows'] = $this->m_model->tampil_data_user()->num_rows();
        $config['per_page'] = 5;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li class="page-item page-link">';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="page-item page-link">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li class="page-item page-link">';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li class="page-item page-link">';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li class="page-item page-link">';
        $config['num_tag_close']    = '</li>';

        $this->pagination->initialize($config);
        if ($this->uri->segment(3) != null){
            $halaman = $this->uri->segment(3);
        }else{
            $halaman = 0;
        }
        
        $data['user'] = $this->m_model->tampildatauser($halaman,$config['per_page'])->result();
        $data['halaman'] = $this->pagination->create_links();
        $this->load->view('temp/v_user', $data);
        $this->load->view('pages/footer');
        
        
    }
// ========================================Pagination Pembelian=============================================================================
    
public function page4()
{
    $this->load->view('pages/header');
    
    $config['base_url'] = 'http://localhost/Heloev2/controller/page4';
    $config['total_rows'] = $this->m_model->tampil_data_pembeli()->num_rows();
    $config['per_page'] = 3;
    $config['full_tag_open']    = '<ul class="pagination">';
    $config['full_tag_close']   = '</ul>';
    $config['first_link']       = 'First';
    $config['last_link']        = 'Last';
    $config['first_tag_open']   = '<li class="page-item page-link">';
    $config['first_tag_close']  = '</li>';
    $config['prev_link']        = '&laquo';
    $config['prev_tag_open']    = '<li class="page-item page-link">';
    $config['prev_tag_close']   = '</li>';
    $config['next_link']        = '&raquo';
    $config['next_tag_open']    = '<li class="page-item page-link">';
    $config['next_tag_close']   = '</li>';
    $config['last_tag_open']    = '<li class="page-item page-link">';
    $config['last_tag_close']   = '</li>';
    $config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
    $config['cur_tag_close']    = '</a></li>';
    $config['num_tag_open']     = '<li class="page-item page-link">';
    $config['num_tag_close']    = '</li>';

    $this->pagination->initialize($config);
    if ($this->uri->segment(3) != null){
        $halaman = $this->uri->segment(3);
    }else{
        $halaman = 0;
    }
    
    $data['user']= $this->m_model->tampildatapembeli($halaman,$config['per_page'])->result();
    $data['halaman'] = $this->pagination->create_links();
    $this->load->view('temp/v_pembelian', $data);
    $this->load->view('pages/footer');
}

public function page5()
{
    $this->load->view('pages/header');
    
    $config['base_url'] = 'http://localhost/Heloev2/controller/page5';
    $config['total_rows'] = $this->m_model->tampil_data_tiket()->num_rows();
    $config['per_page'] = 2;
    $config['full_tag_open']    = '<ul class="pagination">';
    $config['full_tag_close']   = '</ul>';
    $config['first_link']       = 'First';
    $config['last_link']        = 'Last';
    $config['first_tag_open']   = '<li class="page-item page-link">';
    $config['first_tag_close']  = '</li>';
    $config['prev_link']        = '&laquo';
    $config['prev_tag_open']    = '<li class="page-item page-link">';
    $config['prev_tag_close']   = '</li>';
    $config['next_link']        = '&raquo';
    $config['next_tag_open']    = '<li class="page-item page-link">';
    $config['next_tag_close']   = '</li>';
    $config['last_tag_open']    = '<li class="page-item page-link">';
    $config['last_tag_close']   = '</li>';
    $config['cur_tag_open']     = '<li class="active"><a href="" class="page-link">';
    $config['cur_tag_close']    = '</a></li>';
    $config['num_tag_open']     = '<li class="page-item page-link">';
    $config['num_tag_close']    = '</li>';

    $this->pagination->initialize($config);
    if ($this->uri->segment(3) != null){
        $halaman = $this->uri->segment(3);
        
    }else{
        $halaman = 0;
    }
    
    $data['user']= $this->m_model->tampildatatiket($halaman,$config['per_page'])->result();
    $data['halaman'] = $this->pagination->create_links();
    $this->load->view('temp/v_tiket', $data);
    $this->load->view('pages/footer');
    
    
}




    public function awal(){
        $this->load->view('temp/v_login');
    }

    public function menu()
    {
        $data['data_event'] = $this->m_model->CountEvent()->result();
        // print_r($data);
        $data['data_event_valid'] = $this->m_model->CountEventValid();
        $data['data_event_belum_valid'] = $this->m_model->CountEventBelumValid();

        $data['data_user'] = $this->m_model->CountUser();
        $data['data_user_berhasil'] = $this->m_model->CountUserBerhasil();
        $data['data_user_belum_berhasil'] = $this->m_model->CountUserBelumBerhasil();

        $data['data_event_cancel'] = $this->m_model->CountCancelEvent()->num_rows();
		$data['data_event_butuh_approve'] = $this->m_model->CountApprovalCancelEvent()->num_rows();

        $this->load->view('pages/header');
        $this->load->view('temp/v_dashboard',$data);
        $this->load->view('pages/footer');
        
    }
    public function dashboard()
    {
        $data['data_event'] = $this->m_model->CountEvent()->result();
        $data['data_event_valid'] = $this->m_model->CountEventValid();
        $data['data_event_belum_valid'] = $this->m_model->CountEventBelumValid();
        $data['data_event_cancel'] = $this->m_model->CountCancelEvent()->num_rows();
		$data['data_event_butuh_approve'] = $this->m_model->CountApprovalCancelEvent()->num_rows();
        $data['data_user'] = $this->m_model->CountUser();
        $data['data_user_berhasil'] = $this->m_model->CountUserBerhasil();
        $data['data_user_belum_berhasil'] = $this->m_model->CountUserBelumBerhasil();
        $this->load->view('pages/header');
        $this->load->view('temp/v_dashboard',$data);
        $this->load->view('pages/footer');      
    }
    public function registrasi()
    {
        $this->load->view('temp/v_registrasi');
        
    }

    public function listkaryawan()
    {
        $this->load->view('pages/header');
        $this->load->view('temp/v_karyawan');
        $this->load->view('pages/footer');
        
    }
    // =================================================================================================================
    
    

    // =================================================================================================================
    function tambah_aksi(){
        // $ID = $this->input->post('ID');
		$Nama = $this->input->post('Nama');
		$Email = $this->input->post('Email');
		$Password = $this->input->post('Password');
		$Alamat = $this->input->post('Alamat');
 
        $this->form_validation->set_rules('Nama', 'required|max_length[20]');
        $this->form_validation->set_rules('Email', 'required|valid_email');
        // if ($this->form_validation->run() == FALSE)
        // {
        //     $this->load->view('temp/v_registrasi');
        //     redirect('controller/awal');
        // }
        // else
        // {
            // load success template...
            echo "It's all Good!";
            $data = array(
                'Nama' => $Nama,
                'Email' => $Email,
                'Password' => md5($Password),
                'Alamat' => $Alamat
                );
                
            $this->m_model->input_data($data,'admin');
            redirect('controller/awal');
            
        // }

	}
    // =================================================================================================================
    
    function aksi_login(){
        $Email = $this->input->post('Email');
		$Password = $this->input->post('Password');
        $this->form_validation->set_rules('Email', 'required|valid_email');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('temp/v_login');
        }
        else
        {
        }

        $cek = $this->m_model->cek_login($Email,md5($Password));
        $hasil = count($cek);
        $cekadmin = $this->m_model->cek_login_admin($Email,md5($Password));
        $hasiladmin = count($cek);
        
        
        echo "<pre>";
        print_r ($cekadmin[0]['Password']);
        echo "</pre>";
        
        // if ($Email == 'admin@admin.com' && $Password == 'admin') {
        //     redirect('controller/dashboard');
            
        // }
        if($Email==$cekadmin[0]['Email'] && md5($Password)==$cekadmin[0]['Password']){
            
            $nama= $cekadmin[0]['Nama'];
            $data_session = array(
                'nama' => $nama,
                'id'
             	);
            $this->session->set_userdata($data_session);
			redirect('controller/dashboard');
 
		}
		elseif($hasil > 0){
            
            $nama= $cek[0]['Nama'];
            $data_session = array(
                'nama' => $nama,
                'ID' => $cek[0]['ID'],
                'email' => $cek[0]['Email']
              	);
            $this->session->set_userdata($data_session);
			redirect('controller_eo/menu');
 
		}else{
            echo "Username dan password salah !";
            
        //    $this->load->view('temp/v_login');
           
            
		}
    }

    public function aksi_logout(){
        $this->session->sess_destroy();
        redirect('controller_eo/awal');
        $error['error'] = "ANDA BERHASIL LOGOUT";
    }

    // =================================================================================================================
    
    function tampilkaryawan(){
       
        $this->load->view('pages/header');
        $data['user'] = $this->m_model->tampil_data()->result();
		$this->load->view('temp/v_karyawan',$data);
        $this->load->view('pages/footer');
    }

    public function cari(){
        $this->load->view('pages/header');
		$data['user'] = $this->m_model->cariorang();
        $this->load->view('temp/v_karyawan', $data);
        $this->load->view('pages/footer');
	}

    // =================================================================================================================
    
    function tampilevent(){
       
        $this->load->view('pages/header');
        $data['user'] = $this->m_model->tampil_data_event()->result();
		$this->load->view('temp/v_penyelenggara',$data);
        $this->load->view('pages/footer');
    }
    public function carievent(){
        $this->load->view('pages/header');
		$data['user'] = $this->m_model->carievent();
        $this->load->view('temp/v_penyelenggara', $data);
        $this->load->view('pages/footer');
	}
// =================================================================================================================
    
    function tampiluser(){
       
        $this->load->view('pages/header');
        $data['user'] = $this->m_model->tampil_data_user()->result();
        $this->load->view('temp/v_user',$data);
        $this->load->view('pages/footer');
}
    public function cariuser(){
        $this->load->view('pages/header');
		$data['user'] = $this->m_model->cariuser();
        $this->load->view('temp/v_user', $data);
        $this->load->view('pages/footer');
    }
    
    // =================================================================================================================
    
    function tampilpembeli(){
       
        $this->load->view('pages/header');
        $data['user'] = $this->m_model->tampil_data_pembeli()->result();
        $this->load->view('temp/v_pembelian',$data);
        $this->load->view('pages/footer');
}
public function caripembeli(){
    $this->load->view('pages/header');
    $data['user'] = $this->m_model->caripembeli();
    $this->load->view('temp/v_pembelian', $data);
    $this->load->view('pages/footer');
}

function tampiltiket(){
       
    $this->load->view('pages/header');
    $data['user'] = $this->m_model->tampil_data_tiket()->result();
    $this->load->view('temp/v_tiket',$data);
    $this->load->view('pages/footer');
}
public function caritiket(){
    $this->load->view('pages/header');
    $data['user'] = $this->m_model->caritiket();
    $this->load->view('temp/v_tiket', $data);
    $this->load->view('pages/footer');
}
   
    // =================================================================================================================
    
    function hapus($ID){
		$where = array('ID' => $ID);
		$this->m_model->hapus_data_karyawan($where,'petugas');
		redirect('controller/tampilkaryawan');
	}
    // ====================================================================================================================
    // function hapusevent($ID_event){
	// 	$where = array('ID_event' => $ID_event);
	// 	$this->m_model->hapus_data($where,'registrasievent');
	// 	redirect('controller/tampilevent');
    // }
    
    public function hapusevent($ID_event)
    {
        $this->m_model->hapus_data($ID_event);
        // $this->session->set_flashdata('flash', 'Dihapus');
        redirect('controller/tampilevent');
    } 
    public function hapususer($ID_user)
    {
        $this->m_model->hapus_data_user($ID_user);
        // $this->session->set_flashdata('flash', 'Dihapus');
        redirect('controller/tampiluser');
    } 

    // =================================================================================================================
   
    function edit($ID){
        $this->load->view('pages/header');
		$where = array('ID' => $ID);
        $data['user'] = $this->m_model->edit_petugas($where,'petugas')->result();
		$this->load->view('temp/v_edit',$data);
        $this->load->view('pages/footer');
    }
    
    function editevent($ID_event){
        $this->load->view('pages/header');
		$where = array('tiket.ID_event' => $ID_event);
        $data['user'] = $this->m_model->edit_data($where,'registrasievent')->result();
		$this->load->view('temp/v_editevent',$data);
        $this->load->view('pages/footer');
    }

    function edituser($ID_user){
        $this->load->view('pages/header');
		$where = array('ID_user' => $ID_user);
		$data['user'] = $this->m_model->edit_data_user($where,'registrasiuser')->result();
		$this->load->view('temp/v_edituser',$data);
        $this->load->view('pages/footer');
    }
    function editpembeli($ID_pembeli){
        $this->load->view('pages/header');
		$where = $ID_pembeli;
        $data['user'] = $this->m_model->edit_data_pembeli($where,'pembeli')->result();
		$this->load->view('temp/v_editpembelian',$data);
        $this->load->view('pages/footer');
    }

// =======================================================================================================================
    function update(){
        $ID = $this->input->post('ID');
        $Nama = $this->input->post('Nama');
        $Email = $this->input->post('Email');
        $Password = $this->input->post('Password');
        $Alamat = $this->input->post('Alamat');
     
        $data = array(
            'Nama' => $Nama,
            'Email' => $Email,
            'Password' => $Password,
            'Alamat' => $Alamat

        );
     
        $where = array(
            'ID' => $ID
        );
     
        $this->m_model->update_data($where,$data,'petugas');
        redirect('controller/tampilkaryawan');
    }

     public function updateevent()
     {
        $ID_event = $this->input->post('ID_event');
        $penyelenggara = $this->input->post('penyelenggara');
        $tgl_acara = $this->input->post('tgl_acara');
        $waktu_mulai_acara = $this->input->post('waktu_mulai_acara');
        $waktu_berakhir_acara = $this->input->post('waktu_berakhir_acara');
        $lokasi = $this->input->post('lokasi');
        
        $harga = $this->input->post('harga');
        $stok_tiket = $this->input->post('stok_tiket');
     
        $data = array(
            'ID_event' => $ID_event,
            'penyelenggara' => $penyelenggara,
            'tgl_acara' => $tgl_acara,
            'waktu_mulai_acara' => $waktu_mulai_acara,
            'waktu_berakhir_acara' => $waktu_berakhir_acara,
            'lokasi' => $lokasi
        );
        $datax = array(
            'harga' => $harga,
            'stok_tiket' => $stok_tiket
        
     );
     $this->m_model->updateeventby($ID_event,$data);
     $this->m_model->updatetiketby($ID_event,$datax);

        redirect('controller/page2');
     }


     public function updateuser()
     {
        $ID_user = $this->input->post('ID_user');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $email = $this->input->post('email');
        $nohp = $this->input->post('nohp');
        
     
        $data = array(
            'ID_user' => $ID_user,
            'username' => $username,
            'password' => $password,
            'email' => $email,
            'nohp' => $nohp
            
        );
     
        $where = array(
            'ID_user' => $ID_user
        );
     
        $this->m_model->update_data($where,$data,'registrasiuser');
        redirect('controller/tampiluser');
     }

     public function updatepembeli()
     {
        $ID_pembeli = $this->input->post('ID_pembeli');
        $status_pembayaran = $this->input->post('status_pembayaran');
        $data = array('status_pembayaran'=>$status_pembayaran);
        $this->m_model->update_data_pembeli($ID_pembeli,$data);
        redirect('controller/page5');
     }

     public function editstatus($id_event)
     {
         $this->m_model->ConfirmStatus($id_event);
         
         redirect('controller/page2','refresh');
         
     }

     public function editstatususer($id_user)
     {
         $this->m_model->ConfirmStatusUser($id_user);
         
         redirect('controller/page3','refresh');
         
     }
    
// =================================================QRCODE===================================================================
    //  public function Invoice()
    //  {
    //     $data['title']= "Invoice Untuk Pembeli";
    //     $this->load->view('render', $data);
        
    // }

    public function QRCode($kodenya)
    {
        QRCode::png(
            $kodenya,
            $outfile = false,
            $level = QR_ECLEVEL_H,
            $size = 4,
            $margin =1

        );
    }
// =================================================QRCODE===================================================================


    public function cetak_event($id)
    {
            $data['event'] = $this->m_model->get_event1($id);

            $this->load->view('temp/printpdf', $data);
    } 

    public function cetak_tiket($id)
    {
            $data['tiket'] = $this->m_model->get_tiket1($id);
       
            $this->load->view('temp/printtiket', $data);
    } 


// ====================================== Approval Cancel =============================================================

    public function approvalCancel()
    {
        $this->load->view('pages/header');
        $data['user'] = $this->m_model->listCancelEvent()->result();

        $this->load->view('temp/v_approvalPembatalan', $data);
        $this->load->view('pages/footer');
    }

    public function approveCancel($id)
	{
		$data = array(
            'Status' => "Canceled By Admin",
            'berkas_event' => 'null'
		);
		$this->m_model->updateStatus($id, $data);

		//Broadcast Email to Buyers
        $buyers = $this->m_model->getBuyers($id)->result();
		foreach ($buyers as $by) {
			$content = $this->load->view(
				'content_email/content_pembatalan',
				array(
                    'gambar'        => $by->gambar,
					'nama_pembeli'	=> $by->username." [".$by->email."]",
					'id_transaksi'	=> $by->id_transaksi,
					'nama_event' 	=> $by->nama_event,
					'jumlah_tiket'	=> $by->jumlah_tiket,
					'total_dana' 	=> $by->subtotal,
					),
				true
			);
			$this->sendMail($by->email, "HeloEV - PEMBATALAN EVENT", $content);
		}

		redirect(base_url()."controller/approvalCancel");
	}

	public function declineCancel($id)
	{
		$data = array(
			'Status' => "Tervalidasi",
			'alasan' => null
		);
		$this->m_model->updateStatus($id, $data);
		redirect(base_url()."controller/approvalCancel");
	}

	public function sendMail($email, $subject, $content)
	{

		$sendmail = array(
			'email_penerima'	=> $email,
			'subjek'			=> $subject,
			'content'			=> $content
		);

		$send = $this->mailer->send($sendmail);
    }
    
    // ======================================= Detail EVENT PRINT =======================

    public function detail_event($id)
{
    $data['data'] = $this->m_model_eo->get_detail($id)->result();
    
    if(isset($_POST['submit'])){
        error_reporting(0);
        
        $tgl = $this->input->post();
        $data['data'] = $this->m_model_eo->get_detail_date($id, $tgl['dari'], $tgl['sampai'])->result();
        $data['info'] = "Data dari tanggal ".$tgl['dari']." sampai dengan ".$tgl['sampai']." ".count($data['data'])." Records Found";
    }

    $this->load->view('pages/header');
    $this->load->view('temp/v_detailEvent', $data);
    $this->load->view('pages/footer');

}

}

/* End of file Controllername.php */


?>
